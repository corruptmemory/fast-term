#/usr/bin/env bash

font_flags="-lfreetype -I /usr/include/freetype2"
warning_flags="-Wall -Wextra -Wno-unused-parameter -Wno-unused-result -Wno-missing-field-initializers"
opengl_flags="-lGL -lGLU"

clang fastterm.cpp -o fastterm -O3 -lX11 -lm -lrt -lutil -lXft -lXrender -pthread $opengl_flags $warning_flags $font_flags $extra_flags
