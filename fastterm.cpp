
#define LOG_DEBUG

#include "fastterm_types.h"
#include "fastterm_logging.cpp"
#include "fastterm_bytebuffer.cpp"
#include "fastterm_scrollback.cpp"
#include "fastterm_terminal.cpp"
#include <sys/epoll.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
//#include <GL/glxint.h>
// #include <time.h>
#include <X11/Xutil.h>
#include "fastterm_opengl.cpp"

#define MAX_EVENTS 10

int main(int argc, char **argv) {

  Display                 *dpy;
  Window                  root;
  GLint                   visual_attribs[] = {
    GLX_X_RENDERABLE    , GL_TRUE,
    GLX_DRAWABLE_TYPE   , GLX_WINDOW_BIT,
    GLX_RENDER_TYPE     , GLX_RGBA_BIT,
    GLX_X_VISUAL_TYPE   , GLX_TRUE_COLOR,
    GLX_RED_SIZE        , 8,
    GLX_GREEN_SIZE      , 8,
    GLX_BLUE_SIZE       , 8,
    GLX_ALPHA_SIZE      , 8,
    GLX_DEPTH_SIZE      , 24,
    GLX_STENCIL_SIZE    , 8,
    GLX_DOUBLEBUFFER    , GL_TRUE,
    None
  };
  XVisualInfo             *vi;
  Colormap                cmap;
  XSetWindowAttributes    swa;
  Window                  win;
  GLXContext              glc;
  XWindowAttributes       gwa;
  XEvent                  xev;

  dpy = XOpenDisplay(NULL);
 
  if(dpy == NULL) {
    log_error("Cannot connect to X server");
    exit(-1);
  }

  s32 fbcount = 0;
  auto fbc = glXChooseFBConfig(dpy, XDefaultScreen(dpy), visual_attribs, &fbcount);
  if (fbc == NULL) {
    log_error("glXChooseFBConfig failed");
    exit(-1);
  }

  s32 best_fbc = -1, best_num_samp = -1;

  for (auto i = 0; i < fbcount; i++) {
    auto fb = fbc[i];
    auto vi1 = glXGetVisualFromFBConfig(dpy, fb);
    if (vi1 != NULL) {
      s32 samp_buf, samples;
      glXGetFBConfigAttrib(dpy, fb, GLX_SAMPLE_BUFFERS, &samp_buf);
      glXGetFBConfigAttrib(dpy, fb, GLX_SAMPLES,        &samples);
      if (best_fbc < 0 || (samp_buf != 0 && samples > best_num_samp)) {
        best_fbc = i;
        best_num_samp = samples;
      }
    }
    XFree(vi1);
  }

  auto chosen_fbc = fbc[best_fbc];
  
  root = DefaultRootWindow(dpy);
  log_debug("root: %d", root);
  vi = glXGetVisualFromFBConfig(dpy, chosen_fbc);

  if (vi == NULL) {
    log_error("No appropriate visual found");
    exit(-1);
  }

  log_info("Visual %x selected", vi->visualid);
  log_info("Depth %d selected", vi->depth);

  cmap = XCreateColormap(dpy, root, vi->visual, AllocNone);

  swa.colormap = cmap;
  swa.event_mask = ExposureMask | KeyPressMask;

  win = XCreateWindow(dpy, root, 0, 0, 600, 600, 0, vi->depth, InputOutput, vi->visual, CWColormap | CWEventMask, &swa);

  XMapWindow(dpy, win);
  XStoreName(dpy, win, "Fast-term");

  XFree(fbc);

  glc = glXCreateContext(dpy, vi, NULL, GL_TRUE);
  glXMakeCurrent(dpy, win, glc);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
  glEnable(GL_DEBUG_OUTPUT);
  glDebugMessageCallback(message_callback, 0);

  
  program, ok := glLoadShadersFile("shaders/oterm.vert", "shaders/oterm.frag");
  if !ok {
    log.error("Error loading shaders");
    os.exit(-1);
  }

  
  terminal term;
  allocate_terminal(&term, "/usr/bin/fish", 8*1024*1024, 8192, 1024, v2u {.width = 1024, .height = 768 }, 60, 24);
  buffer inpbuf;
  buffer oupbuf;
  struct epoll_event ev, xsock, events[MAX_EVENTS];
  int nfds, epollfd;

  /* Code to set up listening socket, 'listen_sock',
     (socket(), bind(), listen()) omitted. */

  epollfd = epoll_create1(0);
  if (epollfd == -1) {
    log_error("epoll_create1: %d", errno);
    exit(-1);
  }

	int xfd = XConnectionNumber(dpy);
  log_debug("xfd: %d", xfd);
  
  ev.events = EPOLLIN;
  ev.data.fd = term.masterfd;
  if (epoll_ctl(epollfd, EPOLL_CTL_ADD, term.masterfd, &ev) == -1) {
    log_error("epoll_ctl: %d", errno);
    exit(-1);
  }
  xsock.events = EPOLLIN;
  xsock.data.fd = xfd;
  if (epoll_ctl(epollfd, EPOLL_CTL_ADD, STDIN_FILENO, &xsock) == -1) {
    log_error("epoll_ctl: %d", errno);
    exit(-1);
  }
  
  
  for (;;) {
    nfds = epoll_wait(epollfd, events, MAX_EVENTS, 1000);
    if (nfds == -1) {
      log_error("epoll_wait: %d", errno);
      return 1;
    }
    for (auto n = 0; n < nfds; ++n) {
      if (events[n].data.fd == term.masterfd) {
        // log_debug("fish");
        get_next_writable_range(&term.buffer, &inpbuf, MAX_WRITEABLE);
        auto r = read(term.masterfd, inpbuf.data, inpbuf.count);
        if (r <= 0) {
          /* This is not necessarily an error but also happens
           * when the child exits normally. */
          log_error("Nothing to read from child: %d", errno);
          return 1;
        }
        commit_write(&term.buffer, r);
        get_next_readable_range(&term.buffer, &oupbuf);
        for (u32 i = 0; i < oupbuf.count; i++) {
          printf("%c", oupbuf.data[i]);
        }
        commit_read(&term.buffer, oupbuf.count);
      } else if (events[n].data.fd == xfd) {
        // log_debug("X");
        while (XPending(dpy)) {
          XNextEvent(dpy, &xev);
          switch (xev.type) {
          case Expose:
            XGetWindowAttributes(dpy, win, &gwa);
            glViewport(0, 0, gwa.width, gwa.height);
            glClearColor(0.2, 0.3, 0.3, 1.0);
            glClear(GL_COLOR_BUFFER_BIT);
            glXSwapBuffers(dpy, win);
            continue;
          case KeyPress:
            char buf[32];
            int i, num;
            KeySym ksym;
            num = XLookupString(&xev.xkey, buf, sizeof(buf), &ksym, 0);
            for (i = 0; i < num; i++)
              write(term.masterfd, &buf[i], 1);

            continue;
          }
        }
        // get_next_writable_range(&term.buffer, &inpbuf, MAX_WRITEABLE);
        // auto r = read(STDIN_FILENO, inpbuf.data, inpbuf.count);
        // if (r <= 0) {
        //   /* This
        //      is not necessarily an error but also happens
        //    * when the child exits normally. */
        //   log_error("Nothing to read from child: %d", errno);
        //   return 1;
        // }
        // commit_write(&term.buffer, r);
        // get_next_readable_range(&term.buffer, &oupbuf);
        // write(term.masterfd, oupbuf.data, oupbuf.count);
        // commit_read(&term.buffer, oupbuf.count);        
      }
    }
  }

  return 0;
}
