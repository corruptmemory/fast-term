
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <assert.h>

#define MAX_WRITEABLE ((u32)-1)

typedef struct byte_buffer {
  u8 *data;
  u32 full_size;
  u32 data_size;
  u32 start;
  u32 end;
} byte_buffer;


internal b32 allocate_byte_buffer(byte_buffer *const buf, u32 size) {
  zero(buf, byte_buffer);
  auto page_size = sysconf(_SC_PAGESIZE);
  if (page_size == -1) {
    log_error("Error: could not get page size: %d", errno);
    return 0;
  }
  auto sz = (size + page_size - 1) & ~(page_size - 1);
  auto fd = open("/tmp", O_TMPFILE | O_RDWR, S_IRUSR | S_IWUSR);
  if (fd == -1) {
    log_error("Error: could not create temporary file for memory map: %d", errno);
    return 0;
  }
  if (ftruncate(fd, sz)) {
    log_error("Error: could not size memory map file[%d]: %d", sz, errno);
    return 0;
  }
  
  auto ptr = mmap(NULL, sz, PROT_READ | PROT_WRITE, MAP_SHARED_VALIDATE | MAP_LOCKED, fd, 0);
  if (ptr == MAP_FAILED) {
    close(fd);
    log_error("Error: could not memory map temp file: %d", errno);
    return 0;
  }

  auto start = uintptr(ptr) - sz;
  auto ptr1 = mmap((void *)start, sz, PROT_READ | PROT_WRITE, MAP_SHARED_VALIDATE | MAP_FIXED | MAP_LOCKED, fd, 0);
  if (ptr == MAP_FAILED) {
    close(fd);
    if (munmap(ptr, sz)) {
      log_error("Error: could not unmap memory for first pointer: %d", errno);
    }
    log_error("Error: could not memory map second pointer: %d", errno);
    return 0;
  }
  close(fd);

  buf->data = (u8*)ptr1;
  buf->full_size = 2*sz;
  buf->data_size = sz;
  
  
  return 1;
}

internal void delete_byte_buffer(byte_buffer *const buf) {
  auto ptr1 = buf->data + buf->data_size;
  if (munmap(buf->data, buf->data_size)) {
    log_error("Error: failed to unmap first region: %d", errno);
  }
  if (munmap(ptr1, buf->data_size)) {
    log_error("Error: failed to unmap second region: %d", errno);
  }
  zero(buf, byte_buffer);
}

internal void get_next_readable_range(byte_buffer *const buf, buffer *const result) {
  zero(result, buffer);
  assert(buf->start < buf->data_size);
  assert(buf->start <= buf->end);
  result->data = buf->data + buf->start;
  result->count = buf->end - buf->start;
}

internal u32 write_data(buffer *const dst, buffer *const src) {
  u32 amt = dst->count <= src->count ? dst->count : src->count;
  memmove(dst->data, src->data, amt);
  return amt;
}

internal void get_next_writable_range(byte_buffer *const buf, buffer *const result, u32 up_to) {
  assert(buf->start < buf->data_size);
  u32 slice_length = buf->data_size - (buf->end - buf->start);
  if (slice_length > up_to) {
    slice_length = up_to;
  }
  result->data = buf->data + buf->end;
  result->count = slice_length;
}

internal void commit_write(byte_buffer *const buf, u32 size) {
  assert(buf->start < buf->data_size);
  assert(size <= buf->data_size);
  
  buf->end += size;
  
  assert((buf->end - buf->start) <= buf->data_size);
}

internal void commit_read(byte_buffer *const buf, u32 size) {
  assert(buf->start < buf->data_size);
  assert(size <= buf->data_size);
  
  buf->start += size;
  if (buf->start >= buf->data_size) {
    buf->start -= buf->data_size;
  }
  if (buf->end >= buf->data_size) {
    buf->end -= buf->data_size;
  }
  
  assert(buf->start < buf->data_size);
  assert(buf->end < buf->data_size);
  assert(buf->start <= buf->end);
}
