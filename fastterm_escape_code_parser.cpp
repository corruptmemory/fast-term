
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>

typedef enum char_flags {
  CF_BOLD      = 0x00000001,
  CF_UNDERLINE = 0x00000002
} char_flags;

typedef enum color_index {
  COLOR_BLACK,
  COLOR_RED,
  COLOR_GREEN,
  COLOR_YELLOW,
  COLOR_BLUE,
  COLOR_MAGENTA,
  COLOR_CYAN,
  COLOR_WHITE,
  COLOR_BRIGHT_BLACK,
  COLOR_BRIGHT_RED,
  COLOR_BRIGHT_GREEN,
  COLOR_BRIGHT_YELLOW,
  COLOR_BRIGHT_BLUE,
  COLOR_BRIGHT_MAGENTA,
  COLOR_BRIGHT_CYAN,
  COLOR_BRIGHT_WHITE
} color_index;

typedef enum escape_sequence_parser_state {
  ESP_READY,
  ESP_INCOMPLETE,
  ESP_COMPLETE,
  ESP_ERROR
} escape_sequence_parser_state;

typedef enum escape_sequence_parser_result_type {
  ESPR_INVALID,
  ESPR_CHAR_ATTRIBUTE_RESET_ALL,
  ESPR_CHAR_ATTRIBUTE_SET,
  ESPR_CHAR_ATTRIBUTE_RESET,
  ESPR_FONT,
  ESPR_DEFAULT_FOREGROUND_COLOR,
  ESPR_DEFAULT_BACKGROUND_COLOR,
  ESPR_FOREGROUND_16_BIT_COLOR,
  ESPR_BACKGROUND_16_BIT_COLOR,
  ESPR_FOREGROUND_256_BIT_COLOR,
  ESPR_BACKGROUND_256_BIT_COLOR,
  ESPR_FOREGROUND_TRUE_COLOR,
  ESPR_BACKGROUND_TRUE_COLOR,
  ESPR_ERASE_SCREEN,
  ESPR_ERASE_LINE,
  ESPR_MOVE_RELATIVE,
  ESPR_MOVE_HORIZONTAL_ABSOLUTE,
  ESPR_MOVE_ABSOLUTE
} escape_sequence_parser_result_type;



const u32 color_cube[256] = {
  /* Base 16 colors */
  0x00000000, 0x00800000, 0x00008000, 0x00808000, 0x00000080, 0x00800080,
  0x00008080, 0x00c0c0c0, 0x00808080, 0x00ff0000, 0x0000ff00, 0x00ffff00,
  0x000000ff, 0x00ff00ff, 0x0000ffff, 0x00ffffff,

  /* 216 color RGB cube */
  0x00000000, 0x0000005f, 0x00000087, 0x000000af, 0x000000d7, 0x000000ff,
  0x00005f00, 0x00005f5f, 0x00005f87, 0x00005faf, 0x00005fd7, 0x00005fff,
  0x00008700, 0x0000875f, 0x00008787, 0x000087af, 0x000087d7, 0x000087ff,
  0x0000af00, 0x0000af5f, 0x0000af87, 0x0000afaf, 0x0000afd7, 0x0000afff,
  0x0000d700, 0x0000d75f, 0x0000d787, 0x0000d7af, 0x0000d7d7, 0x0000d7ff,
  0x0000ff00, 0x0000ff5f, 0x0000ff87, 0x0000ffaf, 0x0000ffd7, 0x0000ffff,
  0x005f0000, 0x005f005f, 0x005f0087, 0x005f00af, 0x005f00d7, 0x005f00ff,
  0x005f5f00, 0x005f5f5f, 0x005f5f87, 0x005f5faf, 0x005f5fd7, 0x005f5fff,
  0x005f8700, 0x005f875f, 0x005f8787, 0x005f87af, 0x005f87d7, 0x005f87ff,
  0x005faf00, 0x005faf5f, 0x005faf87, 0x005fafaf, 0x005fafd7, 0x005fafff,
  0x005fd700, 0x005fd75f, 0x005fd787, 0x005fd7af, 0x005fd7d7, 0x005fd7ff,
  0x005fff00, 0x005fff5f, 0x005fff87, 0x005fffaf, 0x005fffd7, 0x005fffff,
  0x00870000, 0x0087005f, 0x00870087, 0x008700af, 0x008700d7, 0x008700ff,
  0x00875f00, 0x00875f5f, 0x00875f87, 0x00875faf, 0x00875fd7, 0x00875fff,
  0x00878700, 0x0087875f, 0x00878787, 0x008787af, 0x008787d7, 0x008787ff,
  0x0087af00, 0x0087af5f, 0x0087af87, 0x0087afaf, 0x0087afd7, 0x0087afff,
  0x0087d700, 0x0087d75f, 0x0087d787, 0x0087d7af, 0x0087d7d7, 0x0087d7ff,
  0x0087ff00, 0x0087ff5f, 0x0087ff87, 0x0087ffaf, 0x0087ffd7, 0x0087ffff,
  0x00af0000, 0x00af005f, 0x00af0087, 0x00af00af, 0x00af00d7, 0x00af00ff,
  0x00af5f00, 0x00af5f5f, 0x00af5f87, 0x00af5faf, 0x00af5fd7, 0x00af5fff,
  0x00af8700, 0x00af875f, 0x00af8787, 0x00af87af, 0x00af87d7, 0x00af87ff,
  0x00afaf00, 0x00afaf5f, 0x00afaf87, 0x00afafaf, 0x00afafd7, 0x00afafff,
  0x00afd700, 0x00afd75f, 0x00afd787, 0x00afd7af, 0x00afd7d7, 0x00afd7ff,
  0x00afff00, 0x00afff5f, 0x00afff87, 0x00afffaf, 0x00afffd7, 0x00afffff,
  0x00d70000, 0x00d7005f, 0x00d70087, 0x00d700af, 0x00d700d7, 0x00d700ff,
  0x00d75f00, 0x00d75f5f, 0x00d75f87, 0x00d75faf, 0x00d75fd7, 0x00d75fff,
  0x00d78700, 0x00d7875f, 0x00d78787, 0x00d787af, 0x00d787d7, 0x00d787ff,
  0x00d7af00, 0x00d7af5f, 0x00d7af87, 0x00d7afaf, 0x00d7afd7, 0x00d7afff,
  0x00d7d700, 0x00d7d75f, 0x00d7d787, 0x00d7d7af, 0x00d7d7d7, 0x00d7d7ff,
  0x00d7ff00, 0x00d7ff5f, 0x00d7ff87, 0x00d7ffaf, 0x00d7ffd7, 0x00d7ffff,
  0x00ff0000, 0x00ff005f, 0x00ff0087, 0x00ff00af, 0x00ff00d7, 0x00ff00ff,
  0x00ff5f00, 0x00ff5f5f, 0x00ff5f87, 0x00ff5faf, 0x00ff5fd7, 0x00ff5fff,
  0x00ff8700, 0x00ff875f, 0x00ff8787, 0x00ff87af, 0x00ff87d7, 0x00ff87ff,
  0x00ffaf00, 0x00ffaf5f, 0x00ffaf87, 0x00ffafaf, 0x00ffafd7, 0x00ffafff,
  0x00ffd700, 0x00ffd75f, 0x00ffd787, 0x00ffd7af, 0x00ffd7d7, 0x00ffd7ff,
  0x00ffff00, 0x00ffff5f, 0x00ffff87, 0x00ffffaf, 0x00ffffd7, 0x00ffffff,

  /* 24 Grayscale colors*/
  0x00080808, 0x00121212, 0x001c1c1c, 0x00262626, 0x00303030, 0x003a3a3a,
  0x00444444, 0x004e4e4e, 0x00585858, 0x00626262, 0x006c6c6c, 0x00767676,
  0x00808080, 0x008a8a8a, 0x00949494, 0x009e9e9e, 0x00a8a8a8, 0x00b2b2b2,
  0x00bcbcbc, 0x00c6c6c6, 0x00d0d0d0, 0x00dadada, 0x00e4e4e4, 0x00eeeeee
};

#define EPS_BUFFER_SIZE 255

typedef struct escape_parser_state {
  escape_sequence_parser_state       state;
  escape_sequence_parser_result_type result_type;
  u32                                *index_colors;
  u32 length;
  u8 buffer[EPS_BUFFER_SIZE+1];
  union {
    u32 char_flags;
    u32 font_index;
    u8  erase;
    union {
      struct {
        u8 b, g, r, u;
      } rgb;
      u32 packed;
    } color;
    struct {
      s16 dx, dy;
    } relative;
    struct {
      u16 x, y;
    } absolute;
  };
} escape_parser_state;

internal void reset_escape_parser_state(escape_parser_state *s) {
  zero(s, escape_parser_state);
}

internal void parse_relative(escape_parser_state *s, u8 code) {
  int num = atoi((const char *)s->buffer);
  if (num >= 0 && num < 32768) {
    s->state = ESP_COMPLETE;
    s->result_type = ESPR_MOVE_RELATIVE;
    switch(code) {
    case 'A':
    case 'F':
      s->relative.dy = (s16) -num;
      break;
    case 'B':
    case 'E':
      s->relative.dy = (s16) num;
      break;
    case 'C':
      s->relative.dx = (s16) num;
      break;
    case 'D':
      s->relative.dx = (s16) -num;
      break;
    }
  } else {
    s->state = ESP_ERROR;
  }
}

internal void parse_horizontal_absolute(escape_parser_state *s) {
  int num = atoi((const char *)s->buffer);
  if (num >= 0 && num < 65536) {
    s->state = ESP_COMPLETE;
    s->result_type = ESPR_MOVE_HORIZONTAL_ABSOLUTE;
    s->absolute.x = (u16) num;
  } else {
    s->state = ESP_ERROR;
  }
}

internal void parse_erase_display(escape_parser_state *s) {
  int num = atoi((const char *)s->buffer);
  if (num >= 0 && num <= 3) {
    s->state = ESP_COMPLETE;
    s->result_type = ESPR_ERASE_SCREEN;
    s->erase = (u8) num;
  } else {
    s->state = ESP_ERROR;
  }
}

internal void parse_erase_line(escape_parser_state *s) {
  int num = atoi((const char *)s->buffer);
  if (num >= 0 && num <= 2) {
    s->state = ESP_COMPLETE;
    s->result_type = ESPR_ERASE_LINE;
    s->erase = (u8) num;
  } else {
    s->state = ESP_ERROR;
  }
}

internal void parse_absolute_position(escape_parser_state *s, u8 code) {
  char *ce = NULL;
  long num1 = strtol((const char *)s->buffer, &ce, 10);
  if (*ce != ';' || num1 < 0 || num1 > 65535) {
    s->state = ESP_ERROR;
    return;
  }
  long num2 = strtol(ce+1, NULL, 10);
  if (num2 < 0 || num2 > 65535) {
    s->state = ESP_ERROR;
    return;
  }
  s->state = ESP_COMPLETE;
  s->result_type = ESPR_MOVE_ABSOLUTE;
  s->absolute.y = num1 != 0 ? (u16) num1 : 1;
  s->absolute.x = num2 != 0 ? (u16) num2 : 1;
}


internal void parse_extended_color(escape_parser_state *s, long first_code, long second_code, char *start) {
  if (first_code != 38 && first_code != 48) {
    s->state = ESP_ERROR;
    return;
  }
  if (second_code == 5) {
    // 256 color specification
    int color = atoi(start);
    if (color < 0 || color >= 256) {
      s->state = ESP_ERROR;
      return;
    }
    s->state = ESP_COMPLETE;
    switch (first_code) {
    case 38:
      s->result_type = ESPR_FOREGROUND_256_BIT_COLOR;
      break;
    case 48:
      s->result_type = ESPR_BACKGROUND_256_BIT_COLOR;
      break;
    }
    s->color.packed = color_cube[color];
  } else if (second_code == 2) {
    // RGB color specification
    char *next = NULL;
    long r = strtol(start, &next, 10);
    if (r < 0 || r >= 256 || *next != ';') {
      s->state = ESP_ERROR;
      return;
    }
    long g = strtol(next+1, &next, 10);
    if (g < 0 || g >= 256 || *next != ';') {
      s->state = ESP_ERROR;
      return;
    }
    long b = strtol(next+1, &next, 10);
    if (b < 0 || b >= 256 || *next != 'm') {
      s->state = ESP_ERROR;
      return;
    }
    s->state = ESP_COMPLETE;
    switch (first_code) {
    case 38:
      s->result_type = ESPR_FOREGROUND_TRUE_COLOR;
      break;
    case 48:
      s->result_type = ESPR_BACKGROUND_TRUE_COLOR;
      break;
    }
    s->color.rgb.r = r;
    s->color.rgb.g = g;
    s->color.rgb.b = b;
  } else {
    s->state = ESP_ERROR;
  }
}

internal void parse_character_attribute(escape_parser_state *s) {
  char *next = NULL;
  long attribute = strtol((const char*)s->buffer, &next, 10);
  if (attribute >= 10 && attribute <= 19) {
    s->state = ESP_COMPLETE;
    s->result_type = ESPR_FONT;
    s->font_index = attribute - 10;
  } else if (attribute >= 30 && attribute <= 37) {
    s->state = ESP_COMPLETE;
    s->result_type = ESPR_FOREGROUND_16_BIT_COLOR;
    s->color.packed = s->index_colors[COLOR_BLACK + (attribute - 30)];
  } else if (attribute >= 40 && attribute <= 47) {
    s->state = ESP_COMPLETE;
    s->result_type = ESPR_BACKGROUND_16_BIT_COLOR;
    s->color.packed = s->index_colors[COLOR_BLACK + (attribute - 40)];
  } else if (attribute >= 90 && attribute <= 97) {
    s->state = ESP_COMPLETE;
    s->result_type = ESPR_FOREGROUND_16_BIT_COLOR;
    s->color.packed = s->index_colors[COLOR_BRIGHT_BLACK + (attribute - 90)];
  } else if (attribute >= 100 && attribute <= 107) {
    s->state = ESP_COMPLETE;
    s->result_type = ESPR_BACKGROUND_16_BIT_COLOR;
    s->color.packed = s->index_colors[COLOR_BRIGHT_BLACK + (attribute - 100)];
  } else {
    long second = 0;
    switch (attribute) {
    case 0:
      s->state = ESP_COMPLETE;
      s->result_type = ESPR_CHAR_ATTRIBUTE_RESET_ALL;
      break;
    case 1: // Bold
      s->state = ESP_COMPLETE;
      s->result_type = ESPR_CHAR_ATTRIBUTE_SET;
      s->char_flags = CF_BOLD;
      break;
    case 4: // Underline
      s->state = ESP_COMPLETE;
      s->result_type = ESPR_CHAR_ATTRIBUTE_SET;
      s->char_flags = CF_UNDERLINE;
      break;
    case 22: // Normal
      s->state = ESP_COMPLETE;
      s->result_type = ESPR_CHAR_ATTRIBUTE_RESET;
      s->char_flags = ~CF_BOLD;
      break;
    case 24: // Not underlined
      s->state = ESP_COMPLETE;
      s->result_type = ESPR_CHAR_ATTRIBUTE_RESET;
      s->char_flags = ~CF_UNDERLINE;
      break;
    case 39:
      s->state = ESP_COMPLETE;
      s->result_type = ESPR_DEFAULT_FOREGROUND_COLOR;
      break;
    case 49:
      s->state = ESP_COMPLETE;
      s->result_type = ESPR_DEFAULT_BACKGROUND_COLOR;
      break;
    case 38:
    case 48:
      if (*next != ';') {
        s->state = ESP_ERROR;
        return;
      }
      second = strtol(next+1, &next, 10);
      parse_extended_color(s, attribute, second, next+1);
      break;
    default: // Not supported
      s->state = ESP_ERROR;
      log_warn("not a supported character attribute: %d", attribute);
      break;
    }
  }
}


internal escape_sequence_parser_state feed_parser(escape_parser_state *s, u8 byte) {
  switch (s->state) {
  case ESP_INCOMPLETE:
    if (s->length < EPS_BUFFER_SIZE) {
      s->buffer[s->length] = byte;
      s->length++;
      switch(byte) {
      case 'A':
      case 'B':
      case 'C':
      case 'D':
      case 'E':
      case 'F':
        parse_relative(s, byte);
        break;
      case 'G':
        parse_horizontal_absolute(s);
        break;
      case 'J':
        parse_erase_display(s);
        break;
      case 'K':
        parse_erase_line(s);
        break;
      case 'H':
      case 'f':
        parse_absolute_position(s, byte);
        break;
      case 'm':
        parse_character_attribute(s);
        break;
      }
    } else {
      s->state = ESP_ERROR;
    }
    break;
  case ESP_READY:
    if (byte == '[') {
      s->state = ESP_INCOMPLETE;
    } else {
      s->state = ESP_ERROR;
    }
    break;
  case ESP_COMPLETE:
    s->state = ESP_ERROR;
    break;
  case ESP_ERROR:
    break;
  }
  return s->state;
}
