
#include <ft2build.h>
#include FT_FREETYPE_H
#include <math.h>
#include <stdint.h>
#include "fastterm_texture.h"

internal FT_Error glyph_gen_set_face_size(glyph_gen *gen, FT_Face face) {
  return FT_Set_Char_Size(face, gen->char_width, gen->char_height, gen->x_dpi, gen->y_dpi);
}

internal FT_F26Dot6 from_f64(f64 in) {
  double i;
  double f;
  long r = 0;
  f = modf(in, &i);
  log_debug("f: %f, i: %f", f, i);
	r = (long)(f * 64.0);
	r &= 63;
	r |= (long)(i) << 6;
  log_debug("r: %d", r);
  return r;
}

internal void to_ft_render_mode(render_mode in_render_mode, render_mode_result *result) {
  switch (in_render_mode) {
  case NORMAL:
    result->mode = FT_RENDER_MODE_NORMAL;
    result->load_flags = FT_LOAD_RENDER | FT_LOAD_TARGET_NORMAL;
    return;
  case LIGHT:
    result->mode = FT_RENDER_MODE_LIGHT;
    result->load_flags = FT_LOAD_RENDER | FT_LOAD_TARGET_LIGHT;
    return;
  case LCD:
    result->mode = FT_RENDER_MODE_LCD;
    result->load_flags = FT_LOAD_RENDER | FT_LOAD_TARGET_LCD | FT_LOAD_COLOR;
    return;
  }
  result->mode = FT_RENDER_MODE_NORMAL;
  result->load_flags = FT_LOAD_RENDER | FT_LOAD_TARGET_NORMAL;
  return;
}

internal void glyph_gen_init_point_size(glyph_gen *gen,
                                        render_mode render_mode,
                                        f64 point_height,
                                        u32 x_dpi,
                                        u32 y_dpi) {
  FT_F26Dot6 frac_point_size = from_f64(point_height);
  render_mode_result rm;
  to_ft_render_mode(render_mode, &rm);
  gen->render_mode = rm.mode;
  gen->load_flags = rm.load_flags;
  gen->char_width = frac_point_size;
  gen->char_height = frac_point_size;
  gen->x_dpi = x_dpi;
  gen->y_dpi = y_dpi;
}

internal void glyph_gen_init_pixel_size(glyph_gen *gen, render_mode render_mode, u16 pixel_height, u32 x_dpi, u32 y_dpi) {
  auto dpi = y_dpi;
  if (dpi == 0) dpi = x_dpi;
  f64 point_size = ((f64)pixel_height)/((f64)dpi)*72.0;
  glyph_gen_init_point_size(gen, render_mode, point_size, x_dpi, y_dpi);
}

internal u32 bytes_per_pixel(FT_Pixel_Mode pixel_mode) {
  switch (pixel_mode) {
  case FT_PIXEL_MODE_GRAY:
    return 1;
  case FT_PIXEL_MODE_LCD:
    return 3;
  case FT_PIXEL_MODE_BGRA:
    return 4;
  default:
    log_error("Unsupported pixel mode: %d", pixel_mode);
    return UINT32_MAX;
  }
}

internal b32 glyph_gen_get_max_pixel_dims(glyph_gen *gen, FT_Face face, u32 start_char, u32 end_char, max_pixel_dimensions *maxdims) {
  zero(maxdims, max_pixel_dimensions);
  u32 glyph_index = 0;
  u32 current_char = 0;
  for (current_char = FT_Get_First_Char(face, &glyph_index); glyph_index != 0 && current_char < start_char; current_char = FT_Get_Next_Char(face, current_char, &glyph_index)) {}

  for (;glyph_index != 0 && current_char < end_char; current_char = FT_Get_Next_Char(face, current_char, &glyph_index)) {
    FT_Error err = FT_Load_Glyph(face, glyph_index, gen->load_flags);
    if (err != 0) {
      log_error("Failed to load glyph: %d", err);
      return 0;
    }
    if (gen->bytes_per_pixel == 0) {
      gen->bytes_per_pixel = bytes_per_pixel((FT_Pixel_Mode)face->glyph->bitmap.pixel_mode);
    }
    u32 w = face->glyph->bitmap.width;
    u32 h = face->glyph->bitmap.rows;

    if (w > maxdims->max_width) {
      maxdims->max_width = w;
    }
    if (h > maxdims->max_height) {
      maxdims->max_height = h;
    }
  }
  return 1;
}

// TODO: This is definitely wrong!  It is not aligning the character bases but instead aligning the character
//       tops, which is not what I want (nor does anyone want that)
internal b32 glyph_gen_render_glyph(glyph_gen *gen,
                                    FT_Face face,
                                    u32 glyph_index,
                                    glyph_texture *dest,
                                    u32 x_pixel,
                                    u32 y_pixel) {
  FT_Error err = FT_Load_Glyph(face, glyph_index, gen->load_flags);
  if (err != 0) {
    log_error("Failed to load Glyph: %d", err);
    return 0;
  }

  //  u32 glyph_len = abs(face->glyph->bitmap.pitch) * face->glyph->bitmap.rows;
  u8* glyph_bits = face->glyph->bitmap.buffer;
  auto row_end = face->glyph->bitmap.width;
  if (face->glyph->bitmap.width > dest->glyph_byte_width) {
    row_end = dest->glyph_byte_width;
  }
  auto half_width = (row_end/gen->bytes_per_pixel)/2;
  auto fixup = (dest->half_glyph_pixel_width - half_width)*gen->bytes_per_pixel;
  u32 base_idx = x_pixel * gen->bytes_per_pixel + y_pixel * dest->texture_row_bytes_width;
  u32 yfixup = (u32)((s32)dest->ascender_pixels - face->glyph->bitmap_top);
  if (dest->ascender_pixels < face->glyph->bitmap_top) {
    yfixup = 0;
  }
  auto rows = face->glyph->bitmap.rows;
  if (rows > dest->glyph_pixel_height) {
    rows = dest->glyph_pixel_height;
  }
  for (u32 yi = 0; yi < rows; yi++) {
    auto ydest = yi + yfixup;
    auto ds = base_idx + fixup + (ydest * dest->texture_row_bytes_width);
    auto ss = yi * abs(face->glyph->bitmap.pitch);
    row_end = ss + face->glyph->bitmap.width;
    memcpy(dest->buffer + ds, glyph_bits + ss, face->glyph->bitmap.width);
  }
  return 1;
}
