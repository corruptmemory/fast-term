
#include "stb/stb_image.h"
#include "stb/stb_image_write.h"
#include <ft2build.h>
#include FT_FREETYPE_H
#include "fastterm_glyph_generator.cpp"

internal b32 glyph_texture_get_entry(glyph_texture *texture, rune r, glyph_state *state) {
  auto h = rune_hash(r);
  find_glyph_entry_by_hash(texture->glyph_table, h, state);
  if (state->filled_state == EMPTY) {
    auto gi = FT_Get_Char_Index(texture->face, r);
    auto unpacked = unpack_glyph_cache_point(state->gpu_index);
    auto ok = glyph_gen_render_glyph(&texture->generator, texture->face, gi, texture, unpacked.x*texture->glyph_pixel_width, unpacked.y*texture->glyph_pixel_height);
    if (!ok) {
      log_error("Failed to render glyph");
      return 0;
    }

    update_glyph_cache_entry(texture->glyph_table, state->id, FILLED, u16(unpacked.x*texture->glyph_pixel_height), u16(unpacked.y*texture->glyph_pixel_height));
    state->filled_state = FILLED;
  }
  return 1;
}

internal b32 init_glyph_texture(glyph_texture *texture,
                                FT_Face face,
                                f64 point_size,
                                u32 x_dpi,
                                u32 y_dpi,
                                u16 glyphs_across = 64,
                                rune sizing_start = ' ',
                                rune sizing_end = '~',
                                u32 hash_count = 4096,
                                u32 entry_count = 1024,
                                u32 reserved_tile_count = 0) {
  zero(texture, glyph_texture);
  glyph_gen_init_point_size(&texture->generator, LCD, point_size, x_dpi, y_dpi);
  glyph_gen_set_face_size(&texture->generator, face);
  texture->point_size = point_size;
  texture->face = face;
  max_pixel_dimensions maxdims;
  auto ok = glyph_gen_get_max_pixel_dims(&texture->generator, face, sizing_start, sizing_end, &maxdims);
  if (!ok) {
    log_error("error: could not compute the max pixel dimensions");
    return 0;
  }
  auto pixel_width = maxdims.max_width/texture->generator.bytes_per_pixel;
  texture->glyph_pixel_width = pixel_width;
  texture->glyph_byte_width = maxdims.max_width;
  texture->glyph_pixel_height = maxdims.max_height;
  texture->texture_row_bytes_width = maxdims.max_width * glyphs_across;
  texture->texture_pixel_width = texture->texture_row_bytes_width/texture->generator.bytes_per_pixel;
  auto tr = entry_count / glyphs_across;
  if (entry_count % glyphs_across != 0) {
    tr++;
  }
  texture->texture_pixel_height = tr * maxdims.max_height;
  texture->half_glyph_pixel_width = (maxdims.max_width/texture->generator.bytes_per_pixel)/2;
  texture->ascender_pixels = face->ascender >> 6;
  glyph_table_params gt = {
    .hash_count = hash_count,
    .entry_count = entry_count,
    .reserved_tile_count = reserved_tile_count,
    .cache_tile_count_in_x = glyphs_across
  };
  auto footprint = get_glyph_table_footprint(&gt);
  texture->glyph_cache_memory = calloc(footprint,1);
  texture->glyph_table = place_glyph_table_in_memory(&gt, texture->glyph_cache_memory);
  texture->buffer = (u8*)calloc(texture->texture_pixel_height * texture->texture_row_bytes_width, 1);

  for (rune r = sizing_start; r < sizing_end; r++) {
    auto h = rune_hash(r);
    glyph_state e;
    if (!glyph_texture_get_entry(texture, r, &e)) {
      return 0;
    }
  }

  return 1;
}



internal void destroy_glyph_texture(glyph_texture *texture) {
  free(texture->buffer);
  FT_Done_Face(texture->face);
  free(texture->glyph_cache_memory);
}

internal void glyph_texture_write_to_file(glyph_texture *texture, const char *filename) {
  stbi_write_png(filename,
                 texture->texture_pixel_width,
                 texture->texture_pixel_height,
                 texture->generator.bytes_per_pixel,
                 texture->buffer,
                 texture->texture_row_bytes_width);
}
