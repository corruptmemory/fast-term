
#include <string.h>
#include <assert.h>

typedef u32 glyph_hash;
typedef u32 gpu_glyph_index;
typedef v2u glyph_cache_point;

typedef enum glyph_fill_state {
  EMPTY,
  FILLED
} glyph_fill_state;

typedef struct glyph_entry {
  glyph_hash        hash_value;
  u32               next_with_same_hash;
  u32               next_lru;
  u32               prev_lru;
  gpu_glyph_index   gpu_index;
  glyph_fill_state  filled_state;
  u16               dim_x;
  u16               dim_y;
#ifdef DEBUG_LRU
  s32               ordering;
#endif
} glyph_entry;

typedef struct glyph_table_stats {
  u32    hit_count;
  u32    miss_count;
  u32    recycle_count;
} glyph_table_stats;


typedef struct glyph_state {
  u32                 id;
  gpu_glyph_index     gpu_index;
  glyph_fill_state    filled_state;
  u16                 dim_x;
  u16                 dim_y;
} glyph_state;


typedef struct glyph_table {
  glyph_table_stats  stats;
  u32                hash_mask;
  u32               *hash_table;
  u32                hash_table_count;
  glyph_entry       *entries;
  u32                entry_count;
#ifdef DEBUG_LRU
  s32                last_lru_count;
#endif
} glyph_table;


// TODO(jim): It _must_ be true that the number of entries in the glyph cache
//            has to be at least as large as a full screen of text on the
//            largest available screen connected to the computer.  The limiting
//            case is a unique glyph per cell on the screen while in full-screen
//            mode.
typedef struct glyph_table_params {
  u32     hash_count;
  u32     entry_count;
  u32     reserved_tile_count;
  u32     cache_tile_count_in_x;
} glyph_table_params;

internal glyph_hash rune_hash(rune in) {
  return in;
}

internal gpu_glyph_index pack_glyph_cache_point(u32 x, u32 y) {
  return (y << 16) | x;
}

internal glyph_cache_point unpack_glyph_cache_point(gpu_glyph_index p) {
  return glyph_cache_point {
    .x = p & 0xffff,
    .y = p >> 16
  };
}

internal b32 glyph_hashes_are_equal(glyph_hash a, glyph_hash b) {
  return a == b;
}

internal u32* get_slot_pointer(glyph_table *table, glyph_hash run_hash) {
  auto hash_slot = run_hash & table->hash_mask;
  return table->hash_table + hash_slot;
}

internal glyph_entry* get_entry(glyph_table *table, u32 index) {
  return table->entries + index;
}

internal glyph_entry* get_sentinel(glyph_table *table) {
  return table->entries;
}

internal void get_and_clear_stats(glyph_table *table, glyph_table_stats *stats) {
  if (stats != NULL) {
    zero(stats, glyph_table_stats);
    memcpy(stats, &table->stats, sizeof(glyph_table_stats));
  }
  zero(&table->stats, glyph_table_stats);
}

internal void update_glyph_cache_entry(glyph_table *table,
                                       u32 id,
                                       glyph_fill_state new_state,
                                       u16 new_dim_x,
                                       u16 new_dim_y) {
   auto entry = get_entry(table, id);
  entry->filled_state = new_state;
  entry->dim_x = new_dim_x;
  entry->dim_y = new_dim_y;
}

#ifdef DEBUG_LRU
internal void validate_lru(glyph_table *table, s32 expected_count_change) {
  s32 entry_count = 0;
  auto sentinel = get_sentinel(table);
  auto last_ordering = sentinel->ordering;
  for (auto entry_index = sentinel->next_lru; entry_index != 0;) {
    auto entry = get_entry(table, entry_index);
    assert(entry->ordering < last_ordering);
    last_ordering = entry->ordering;
    entry_index = entry->next_lru;
    entry_count++;
  }

  if (table->last_lru_count + expected_count_change != entry_count) {
    log_error("Cache validation failed");
    exit(-1);
  }
  table->last_lru_count = entry_count;
}
#else
#define validate_lru(...)
#endif

internal void recycle_lru(glyph_table *table) {
  auto sentinel = get_sentinel(table);
  assert(sentinel->prev_lru != 0);
  auto entry_index = sentinel->prev_lru;
  auto entry = get_entry(table, entry_index);
  auto prev = get_entry(table, entry->prev_lru);
  prev->next_lru = 0;
  sentinel->prev_lru = entry->prev_lru;
  validate_lru(table, -1);

  u32 *next_index = NULL;
  for (next_index = get_slot_pointer(table, entry->hash_value);
       *next_index != entry_index;
       next_index = &(get_entry(table, *next_index)->next_with_same_hash)) {
    assert(*next_index != 0);
  }

  assert(*next_index == entry_index);
  *next_index = entry->next_with_same_hash;
  entry->next_with_same_hash = sentinel->next_with_same_hash;
  sentinel->next_with_same_hash = entry_index;

  update_glyph_cache_entry(table, entry_index, EMPTY, 0, 0);
  table->stats.recycle_count++;
}

internal u32 pop_free_entry(glyph_table *table) {
  auto sentinel = get_sentinel(table);

  if (sentinel->next_with_same_hash == 0) {
    recycle_lru(table);
  }
  auto result = sentinel->next_with_same_hash;
  assert(result != 0);

  auto entry = get_entry(table, result);
  sentinel->next_with_same_hash = entry->next_with_same_hash;
  entry->next_with_same_hash = 0;

  assert(entry != NULL);
  assert(entry != sentinel);
  assert(entry->dim_x == 0);
  assert(entry->dim_y == 0);
  assert(entry->filled_state == EMPTY);
  assert(entry->next_with_same_hash == 0);
  assert(entry == get_entry(table, result));
  
  return result;
}

internal void find_glyph_entry_by_hash(glyph_table *table, glyph_hash run_hash, glyph_state *state) {
  glyph_entry *result = NULL;

  auto slot = get_slot_pointer(table, run_hash);
  u32 entry_index = 0;
  for (entry_index = *slot; entry_index != 0;) {
    auto entry = get_entry(table, entry_index);
    if (glyph_hashes_are_equal(entry->hash_value, run_hash)) {
      result = entry;
      break;
    }
    entry_index = entry->next_with_same_hash;
  }

  if (result != NULL) {
    assert(entry_index != 0);

    auto prev = get_entry(table, result->prev_lru);
    auto next = get_entry(table, result->next_lru);

    prev->next_lru = result->next_lru;
    next->prev_lru = result->prev_lru;

    validate_lru(table, -1);
    table->stats.hit_count++;
  } else {
    entry_index = pop_free_entry(table);
    assert(entry_index != 0);
    result = get_entry(table, entry_index);
    assert(result->filled_state == EMPTY);
    assert(result->next_with_same_hash == 0);
    assert(result->dim_x == 0);
    assert(result->dim_y == 0);
    result->next_with_same_hash = *slot;
    result->hash_value = run_hash;
    *slot = entry_index;
    table->stats.miss_count++;
  }

  auto sentinel = get_sentinel(table);
  assert(result != sentinel);
  result->next_lru = sentinel->next_lru;
  result->prev_lru = 0;
  auto next_lru = get_entry(table, sentinel->next_lru);
  next_lru->prev_lru = entry_index;
  sentinel->next_lru = entry_index;

#ifdef DEBUG_LRU
  result->ordering = sentinel->ordering;
  sentinel->ordering++;
#endif
  validate_lru(table, 1);

  state->id = entry_index;
  state->dim_x = result->dim_x;
  state->dim_y = result->dim_y;
  state->gpu_index = result->gpu_index;
  state->filled_state = result->filled_state;
}

internal void initialize_direct_glyph_table(glyph_table_params *params,
                                            gpu_glyph_index *table,
                                            size_t table_size,
                                            b32 skip_zero_slot) {
  assert(params->cache_tile_count_in_x >= 1);

  u32 szs = 0;
  if (skip_zero_slot) { szs = 1; }

  u32 x = szs, y = 0;
  for (u32 entry_index = 0; entry_index < (params->reserved_tile_count - szs); entry_index++) {
    if (x >= params->cache_tile_count_in_x) {
      x = 0;
      y++;
    }

    table[entry_index] = pack_glyph_cache_point(x, y);
    x++;
  }
}

internal u32 get_glyph_table_footprint(glyph_table_params *params) {
  auto hash_size = params->hash_count * sizeof(u32);
  auto entry_size = params->entry_count * sizeof(glyph_entry);
  return sizeof(glyph_table) + hash_size + entry_size;
}

internal glyph_table *place_glyph_table_in_memory(glyph_table_params *params, void *memory) {
  assert(params->hash_count >= 1);
  assert(params->entry_count >= 2);
  assert(IsPow2(params->hash_count));
  assert(params->cache_tile_count_in_x >= 1);

  glyph_table* result = NULL;

  if (memory != NULL) {
    glyph_entry *entries = (glyph_entry*) memory;
    result = (glyph_table*)(entries + params->entry_count);
    memset(result, 0, sizeof(*result));
    result->hash_table = (u32*)(result + 1);
    result->hash_table_count = params->hash_count;
    result->entries = entries;
    result->entry_count = params->entry_count;
    result->hash_mask = params->hash_count - 1;
    memset(result->hash_table, 0, params->hash_count*sizeof(*result->hash_table));
    memset(entries, 0, params->entry_count*sizeof(*entries));
    auto starting_tile = params->reserved_tile_count;
    //    auto sentinel = get_sentinel(result);
    auto x = starting_tile % params->cache_tile_count_in_x;
    auto y = starting_tile / params->cache_tile_count_in_x;
    for (u32 entry_index = 0; entry_index < params->entry_count; entry_index++) {
      if (x >= params->cache_tile_count_in_x) {
        x = 0;
        y++;
      }
      auto entry = get_entry(result, entry_index);
      if ((entry_index+1) < params->entry_count) {
        entry->next_with_same_hash = entry_index + 1;
      } else {
        entry->next_with_same_hash = 0;
      }
      entry->gpu_index = pack_glyph_cache_point(x, y);
      entry->filled_state = EMPTY;
      entry->dim_x = 0;
      entry->dim_y = 0;
      x++;
    }
    get_and_clear_stats(result, NULL);
  }
  return result;
}
