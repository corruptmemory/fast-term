#include <stdarg.h>
#include <stdio.h>

typedef enum log_level {
  TEST,
  DEBUG,
  WARN,
  ERROR,
  INFO
} log_level;

internal const char *__test  = "TEST";
internal const char *__debug = "DEBUG";
internal const char *__warn  = "WARN";
internal const char *__error = "ERROR";
internal const char *__info  = "INFO";

internal const char *__log_level(log_level level) {
  switch (level) {
  case TEST:
    return __test;
  case DEBUG:
    return __debug;
  case WARN:
    return __warn;
  case ERROR:
    return __error;
  case INFO:
    return __info;
  }
  // Should never get here.
  return NULL;
}

internal void __log(log_level level, const char *file, int line, const char *format, ...) {
  va_list argp;
  va_start(argp, format);
  printf("%s :: [%s:%d] - ", __log_level(level), file, line);
  vprintf(format, argp);
  printf("\n");
}

#ifdef LOG_TEST
#define LOG_WARN
#define LOG_ERROR
#define LOG_INFO
#define log_test(format, ...) __log(TEST, __FILE__, __LINE__, format, ##__VA_ARGS__)
#else
#define log_test(...)
#endif

#ifdef LOG_DEBUG
#define LOG_WARN
#define LOG_ERROR
#define LOG_INFO
#define log_debug(format, ...) __log(DEBUG, __FILE__, __LINE__, format, ##__VA_ARGS__)
#else
#define log_debug(...)
#endif

#ifdef LOG_WARN
#define LOG_ERROR
#define LOG_INFO
#define log_warn(format, ...) __log(WARN, __FILE__, __LINE__, format, ##__VA_ARGS__)
#else
#define log_warn(...)
#endif

#ifdef LOG_ERROR
#define LOG_INFO
#define log_error(format, ...) __log(ERROR, __FILE__, __LINE__, format, ##__VA_ARGS__)
#else
#define log_error(...)
#endif

#ifdef LOG_INFO
#define log_info(format, ...) __log(INFO, __FILE__, __LINE__, format, ##__VA_ARGS__)
#else
#define log_info(...)
#endif


