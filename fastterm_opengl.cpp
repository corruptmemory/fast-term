
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>


void GLAPIENTRY
message_callback(GLenum source,
                 GLenum type,
                 GLuint id,
                 GLenum severity,
                 GLsizei length,
                 const GLchar* message,
                 const void* userParam ) {
  fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
           ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
            type, severity, message );
}

internal u32 compile_shader_from_source(const char *source, u32 source_length, GLenum shader_type) {
  GLuint shader_id = glCreateShader(shader_type);
  glShaderSource(shader_id, 1, &source, &source_length);
  glCompileShader(shader_id);

  return shader_id;
}

internal u32 create_and_link_program(u32 *shader_ids, u32 shader_count, b32 binary_retrievable) {
    GLuint program_id = glCreateProgram();
    for (u32 i = 0; i < shader_count; i++) {
      glAttachShader(program_id, shader_ids[i]);
    }
    if (binary_retrievable) {
      glProgramParameteri(program_id, GL_PROGRAM_BINARY_RETRIEVABLE_HINT, GL_TRUE);
    }
    glLinkProgram(program_id);
    glValidateProgram(program_id);

    return program_id;
}

internal b32 read_file(const char *file_name, u8 *scratch_buffer, u32 scratch_buffer_size) {
  FILE *f = fopen(file_name, "rb");
  if (f == NULL) {
    log_error("Could not read: %s", file_name);
    return 0;
  }
  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);
  if (fsize >= scratch_buffer_size) {
    log_error("Scratch buffer insuffiently large: %d >= %d", fsize, scratch_buffer_size);
    return 0;
  }
  fseek(f, 0, SEEK_SET);  /* same as rewind(f); */
  
  fread(scratch_buffer, 1, fsize, f);
  fclose(f);
  scratch_buffer[fsize] = 0;
  return 1;
}


internal u32 load_shaders_source(const char *vs_source, const char *fs_source, b32 binary_retrievable) {
  // actual function from here
  u32 vertex_shader_id = compile_shader_from_source(vs_source, GL_VERTEX_SHADER);
  if (vertex_shader_id == 0) {
    log_error("Failed to compile vertex shader");
    return 0;
  }

  u32 fragment_shader_id = compile_shader_from_source(fs_source, GL_FRAGMENT_SHADER);
  if (fragment_shader_id == 0) {
    glDeleteShader(vertex_shader_id);
    log_error("Failed to compile fragment shader");
    return 0;
  }
  u32 args[2] = {vertex_shader_id, fragment_shader_id};
  u32 program_id = create_and_link_program(args, 2, binary_retrievable);
  glDeleteShader(vertex_shader_id);
  glDeleteShader(fragment_shader_id);
  if (program_id == 0) {
    log_error("Failed to link program");
    return 0;
  }

  return program_id;
}


internal u32 load_shaders_file(const char *vertex_source, const char *fragment_source, u8 *scratch_buffer, u32 scratch_buffer_size) {
  u32 sbs = scratch_buffer_size / 2;
  u8 *sb1 = scratch_buffer;
  u8 *sb2 = scratch_buffer + sbs;
  if (!read_file(vs_filename, sb1, sbs)) {
    log_error("Failed to load vertex shader");
    return 0;
  }
  if (!read_file(fs_filename, sb2, sbs)) {
    log_error("Failed to load fragment shader");
    return 0;
  }

  return load_shaders_source(sb1, sb2, binary_retrievable);
}
