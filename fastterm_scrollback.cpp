#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <assert.h>

/*
 * TODO: Remove the EOL flag -- redundant
 */

typedef enum line_flags {
  EOL = 0x01,
} line_flags;

typedef struct line {
  u32 count;
  u32 start;
  u32 end;
  u32 flags;
} line;

typedef struct character_cell {
  union {
    u32 rune;
    u8 byte;
  } c;
  u32 foreground;
  u32 background;
  u32 flags;
  u32 pad1, pad2, pad3, pad4;
} character_cell;


typedef struct character_cell_buffer {
  u32 count;
  character_cell *cells;
} character_cell_buffer;

typedef struct scrollback_buffer {
  character_cell *cells;
  u32   cell_count;
  u32   cells_size;
  line *lines;
  u32   line_count;
  u32   lines_size;
  u32   wrap;
  u32   top_line;
  u32   bottom_line;
} scrollback_buffer;


typedef enum top_bottom_state {
  NORMAL,
  NO_LINES,
  COINCIDENT,
  OVERLAPPING,
  FILLED
} top_bottom_state;


typedef struct line_iterator {
  scrollback_buffer *buffer;
  b32 backwards;
  u32 current;
} line_iterator;

typedef struct line_info {
  character_cell_buffer cells;
  u32 flags;
  u32 index;
} line_info;


// TODO(jim): Not really liking this.  I feel that there is a simplification
//            I'm missing.
internal top_bottom_state compute_top_bottom_state(scrollback_buffer *buf) {
  auto tl = buf->lines + buf->top_line;
  auto bl = buf->lines + buf->bottom_line;
  if (tl == bl) { return tl->count == 0 ? NO_LINES : COINCIDENT; }
  auto tls = tl->start, tle = tl->end;
  auto bls = bl->start, ble = bl->end;
  if (ble == bls && bl->count > 0) { return FILLED; }
  if (tle >= tls) {
    if (ble >= bls) {
      if (bls > tle || ble < tls)  {
        if (bl->count + tl->count > buf->wrap)  { return OVERLAPPING; }
        else                                    { return NORMAL; }
      } else                       { return OVERLAPPING; }
    } else {
      if (ble < tls)               { return NORMAL; }
      else                         { return OVERLAPPING; }
    }
  } else {
    if (ble >= bls && ble < tls)   { return NORMAL; }
    else                           { return OVERLAPPING; }
  }
}

internal b32 allocate_scrollback_buffer(scrollback_buffer *buf, u32 size, u32 max_lines) {
  zero(buf, scrollback_buffer);
  auto page_size = sysconf(_SC_PAGESIZE);
  if (page_size == -1) {
    log_error("could not get page size: %d", errno);
    return 0;
  }

  // Prep the circular-buffer memory for cells
  auto sz = (size + page_size - 1) & ~(page_size - 1);
  assert(sz % sizeof(character_cell) == 0);
  auto fd = open("/tmp", O_TMPFILE | O_RDWR, S_IRUSR | S_IWUSR);
  if (fd == -1) {
    log_error("could not create temporary file for memory map: %d", errno);
    return 0;
  }
  if (ftruncate(fd, sz)) {
    log_error("could not size memory map file[%d]: %d", sz, errno);
    return 0;
  }
  
  auto ptr = mmap(NULL, sz, PROT_READ | PROT_WRITE, MAP_SHARED_VALIDATE | MAP_LOCKED, fd, 0);
  if (ptr == MAP_FAILED) {
    close(fd);
    log_error("could not memory map temp file: %d", errno);
    return 0;
  }

  auto start = uintptr(ptr) - sz;
  auto ptr1 = mmap((void *)start, sz, PROT_READ | PROT_WRITE, MAP_SHARED_VALIDATE | MAP_FIXED | MAP_LOCKED, fd, 0);
  if (ptr == MAP_FAILED) {
    close(fd);
    if (munmap(ptr, sz)) {
      log_error("could not unmap memory for first pointer: %d", errno);
    }
    log_error("could not memory map second pointer: %d", errno);
    return 0;
  }
  close(fd);

  // Prep the memory for the lines buffer
  fd = open("/tmp", O_TMPFILE | O_RDWR, S_IRUSR | S_IWUSR);
  if (fd == -1) {
    if (munmap(ptr, sz)) {
      log_error("could not unmap memory for first pointer: %d", errno);
    }
    if (munmap(ptr1, sz)) {
      log_error("could not unmap memory for second pointer: %d", errno);
    }
    log_error("could not create temporary file for memory map: %d", errno);
    return 0;
  }
  auto lsz = max_lines * sizeof(line);
  lsz = (lsz + page_size - 1) & ~(page_size - 1);
  if (ftruncate(fd, lsz)) {
    if (munmap(ptr, sz)) {
      log_error("could not unmap memory for first pointer: %d", errno);
    }
    if (munmap(ptr1, sz)) {
      log_error("could not unmap memory for second pointer: %d", errno);
    }
    log_error("could not size line memory map file[%d]: %d", lsz, errno);
    return 0;
  }
  auto lptr = mmap(NULL, lsz, PROT_READ | PROT_WRITE, MAP_SHARED_VALIDATE | MAP_LOCKED, fd, 0);
  if (lptr == MAP_FAILED) {
    close(fd);
    if (munmap(ptr, sz)) {
      log_error("could not unmap memory for first pointer: %d", errno);
    }
    if (munmap(ptr1, sz)) {
      log_error("could not unmap memory for second pointer: %d", errno);
    }
    log_error("could not memory map temp file: %d", errno);
    return 0;
  }
  close(fd);

  u32 count = (2 * sz) / sizeof(character_cell);
  assert(count % 2 == 0);

  buf->cells = (character_cell*)ptr1;
  buf->cell_count = count;
  buf->cells_size = sz;

  buf->lines = (line*)lptr;
  buf->line_count = max_lines;
  buf->lines_size = lsz;

  buf->wrap = count/2;

  return 1;
}

internal void reset_scrollback_buffer(scrollback_buffer *buf) {
  memset(buf->cells, 0, sizeof(character_cell) * buf->wrap);
  memset(buf->lines, 0, buf->lines_size);
  buf->top_line = 0;
  buf->bottom_line = 0;
}

internal void delete_scrollback_buffer(scrollback_buffer *buf) {
  auto ptr1 = buf->cells + buf->wrap;
  if (munmap(buf->cells, buf->cells_size)) {
    log_error("failed to unmap first region: %d", errno);
  }
  if (munmap(ptr1, buf->cells_size)) {
    log_error("failed to unmap second region: %d", errno);
  }
  if (munmap(buf->lines, buf->lines_size)) {
    log_error("failed to unmap lines region: %d", errno);
  }
  zero(buf, scrollback_buffer);
}

internal void shift_top_line(scrollback_buffer *buf) {
  auto bl = buf->line_count;
  for (top_bottom_state st = compute_top_bottom_state(buf); st == OVERLAPPING; st = compute_top_bottom_state(buf)) {
    buf->top_line = (buf->top_line + 1) % bl;
  }
  assert(buf->top_line == buf->bottom_line || compute_top_bottom_state(buf) == NORMAL);
}

internal void shift_lines(scrollback_buffer *buf, u32 start_idx, u32 delta) {
  auto bl = buf->line_count;
  auto w = buf->wrap;
  u32 ni = start_idx, i = 0;
  do {
    i = ni;
    auto l = buf->lines + i;
    l->start = (l->start + delta) % w;
    l->end = (l->end + delta) % w;
    ni = (ni + 1) % bl;
  } while(i != buf->bottom_line);
  shift_top_line(buf);
}

internal u32 available_lines(scrollback_buffer *buf) {
  switch (compute_top_bottom_state(buf)) {
  case NORMAL: 
    if (buf->top_line < buf->bottom_line) {
      return buf->bottom_line - buf->top_line + 1;
    }
    return (buf->bottom_line + buf->line_count) - buf->top_line + 1;
  case COINCIDENT:
  case FILLED:
  case NO_LINES:
    return 1;
  default:
    return 0;
  }
}

internal void splice_into_line(scrollback_buffer *const buf,
                               const u32 line_idx,
                               const u32 start,
                               const character_cell_buffer *const data) {
  assert(line_idx < buf->line_count);
  assert(line_idx < available_lines(buf));
  const auto dc = data->count;
  const auto lc = buf->line_count;
  const auto lidx = (buf->top_line + line_idx) % lc;
  const auto l = buf->lines + lidx;
  const auto istart = l->start + start;
  character_cell *cells = data->cells;
  u32 total_count = 0;
  s32 extension = 0;
  if (start < l->count) {
    u32 overlap = l->count - start;
    extension = s32(dc) - s32(overlap);
    total_count = u32(extension + s32(l->count));
    if (total_count < l->count) {
      total_count = l->count;
    }
  } else {
    extension = start - l->count;
    total_count = start + dc;
  }
  if (total_count == l->count) {
    // Overwrite
    memmove(buf->cells + istart, cells, dc * sizeof(character_cell));
  } else if (total_count < buf->wrap) {
    // We are guaranteed to extend in this case
    auto nend = istart + dc - 1;
    auto inc = nend - l->end;
    assert(inc > 0);
    auto bl = buf->lines + buf->bottom_line;
    auto tl = buf->lines + buf->top_line;
    if (lidx == buf->bottom_line || tl == bl) {
      // Overwrite and sorta extend as needed
      memmove(buf->cells + istart, cells, dc * sizeof(character_cell));
      // We have to fill in the gap with spaces
      // TODO: Optimize this
      for (auto i = l->end; i < istart; i++) {
        zero(buf->cells + i, character_cell);
      }
      l->end = nend % buf->wrap;
      l->count = total_count;
      assert(l->count < buf->wrap);
      if (bl != tl) {
        shift_top_line(buf);
      }
    } else {
      auto nlidx = ((lidx + 1) % lc);
      auto l1 = buf->lines + nlidx;
      auto ble = bl->end;
      u32 cells_to_move = 0;
      if (ble < tl->start) {
        cells_to_move = (ble + buf->wrap) - l1->start;
      } else {
        cells_to_move = ble - l1->start;
      }
      // Shift the following lines to make room for the incoming data
      memmove(buf->cells + l1->start + inc, buf->cells + l1->start, cells_to_move * sizeof(character_cell));        
      // Overwrite
      memmove(buf->cells + istart, cells, dc * sizeof(character_cell));
      // We have to fill in the gap with spaces
      // TODO: Optimize this
      for (auto i = l->end; i < istart; i++) {
        zero(buf->cells + i, character_cell);
      }
      l->end = nend % buf->wrap;
      l->count = total_count;
      assert(l->count < buf->wrap);
      shift_lines(buf, nlidx, inc);
    }
  } else if (dc >= buf->wrap) {
    buf->bottom_line = lidx;
    cells = cells + (dc - buf->wrap);
    u32 count = buf->wrap;
    memmove(buf->cells + l->start, cells, count * sizeof(character_cell));
    l->end = l->start;
    l->count = count;
    buf->top_line = buf->bottom_line;
  } else {
    // dc < buf->wrap
    // dc + extension <= buf->wrap
    // extension > 0
    // total_count >= buf->wrap
    // l->count + extension + dc >= buf->wrap
    for (auto i = l->end; i < istart; i++) {
      zero(buf->cells + i, character_cell);
    }
    buf->bottom_line = lidx;
    memmove(buf->cells + istart, cells, dc * sizeof(character_cell));
    l->start= (l->start + total_count) % buf->wrap;
    l->end = l->start;
    l->count = buf->wrap;
    buf->top_line = buf->bottom_line;
  }
}

internal void append_cells(scrollback_buffer *buf, character_cell_buffer *src, b32 eol) {
  auto bl = buf->lines + buf->bottom_line;
  auto blc = buf->line_count;
  if (src != NULL) {
    u32 count = src->count;
    u32 total_count = count + bl->count;
    character_cell *cells = src->cells;
    if (total_count < buf->wrap) {
      memmove(buf->cells + bl->end, cells, count * sizeof(character_cell));
      bl->end = (bl->start + bl->count + count - 1) % buf->wrap;
      bl->count += count;
    } else if (count >= buf->wrap) {
      cells = cells + (count - buf->wrap);
      count = buf->wrap;
      memmove(buf->cells + bl->start, cells, count * sizeof(character_cell));
      bl->end = bl->start;
      bl->count = count;
      buf->top_line = buf->bottom_line;
    } else {
      // total_count > buf->wrap && count < buf->wrap
      // total_count = bl->count + src->count
      // bl->count + (total_count - buf->wrap) + src->count == buf->wrap
      u32 shift = total_count - buf->wrap;
      bl->start = (bl->start + shift) % buf->wrap;
      memmove(buf->cells + bl->end, cells, count * sizeof(character_cell));
      bl->end = bl->start;
      bl->count = buf->wrap;
      buf->top_line = buf->bottom_line;
    }
  }
  if (eol) {
    auto nstart = (bl->end + 1) % buf->wrap;
    bl->flags |= EOL;
    buf->bottom_line = (buf->bottom_line + 1) % blc;
    bl = buf->lines + buf->bottom_line;
    bl->count = 0;
    bl->flags = 0;
    bl->start = nstart;
    bl->end = nstart;
    // We're not allowed to accidentally become coincident with the top line in this context
    if (buf->bottom_line == buf->top_line) {
      buf->top_line = (buf->top_line + 1) % blc;
    }
  }
  shift_top_line(buf);
  assert(buf->top_line == buf->bottom_line || compute_top_bottom_state(buf) == NORMAL);
}


internal void get_line_iterator(scrollback_buffer *buf, line_iterator *it, b32 backwards = 1) {
  zero(it, line_iterator);
  it->buffer = buf;
  it->backwards = backwards;
  it->current = buf->top_line;
  if (backwards) {
    it->current = buf->bottom_line;
  }
}

internal b32 next_line(line_iterator* it, line_info *info) {
  zero(info, line_info);
  auto buf = it->buffer;
  auto le = buf->lines + it->current;
  info->index = it->current;
  info->flags = le->flags;
  info->cells = {
    .count = le->count,
    .cells = buf->cells + le->start
  };
  if (buf->top_line == buf->bottom_line) return 0;
  if (it->backwards) {
    if (it->current == buf->top_line) return 0;
    if (it->current != 0) {
      it->current--;
    } else {
      it->current = buf->line_count - 1;
    }
  } else {
    if (it->current == buf->bottom_line) return 0;
    it->current = (it->current + 1) % buf->line_count;
  }
  return 1;
}
