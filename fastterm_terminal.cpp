
#include <assert.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pwd.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include "fastterm_escape_code_parser.cpp"
const u8 escape = '\x1b';

#define CHARACTER_CELL_SCRATCH_SIZE 256
#define MAX_ROWS 512
#define INVALID_ROW_INDEX u32(-1)

typedef enum parse_state {
  PS_NORMAL,
  PS_VT_CODE,
  PS_UNICODE
} parse_state;

typedef struct row_data {
  u32 line;
  u32 offset;
} row_data;

typedef struct terminal {
  byte_buffer           buffer;
  scrollback_buffer     scrollback;
  const char           *program;
  int                   masterfd;
  int                   child_pid;
  v2u                   cursor;
  v2u                   size;
  u32                   columns;
  u32                   rows;
  parse_state           parse_state;
  escape_parser_state   escape_parser;
  u32                   index_colors[16];
  u32                   default_background_color;
  u32                   default_foreground_color;
  u32                   current_background_color;
  u32                   current_foreground_color;
  u32                   font_index;
  u32                   current_character_attributes;
  character_cell_buffer character_cell_scratch_buffer;
  character_cell        character_cell_scratch[CHARACTER_CELL_SCRATCH_SIZE];
  u32                   top_line;
  row_data              row_data[MAX_ROWS];
} terminal;


internal b32 create_pty(terminal *term) {
	int m;
  //  log_debug("Opening PTY");
  m = posix_openpt(O_RDWR);

	if (m < 0) {
		log_error("openpty failed[%d]: %s", errno, strerror(errno));
    return 0;
  }
  grantpt(m);
  unlockpt(m);
  auto line = ptsname(m);
  auto slave = open(line, O_RDWR, 0);
  char *const args[2] = {
    (char *const) term->program,
    NULL
  };

  auto pid = fork();
  if (pid == 0) {
    close(m);
    setsid(); /* create a new process group */
 		dup2(slave, 0);
		dup2(slave, 1);
		dup2(slave, 2);
    close(slave);
    if (ioctl(0, TIOCSCTTY, 1) == -1) {
      log_error("ioctl(TIOCSCTTY): %d", errno);
      exit(-1);
    }
    execvp(term->program, args);
  }
  term->masterfd = m;
  term->child_pid = pid;
  term->parse_state = PS_NORMAL;
  clear_accumulator(term);
 	return 1;
}


internal void update_terminal_size(terminal *term) {
  struct winsize w = {
    .ws_row = (u16) term->rows,
    .ws_col = (u16) term->columns,
    .ws_xpixel = (u16) term->size.width,
    .ws_ypixel = (u16) term->size.height
  };

  if (ioctl(term->masterfd, TIOCSWINSZ, &w) < 0) {
    log_error("error in update_terminal_size[%d]: %s", errno, strerror(errno));
  }
}

internal b32 allocate_terminal(terminal *term,
                               const char *program,
                               u32 buffer_size,
                               u32 scrollback_buffer_size,
                               u32 max_lines,
                               v2u size,
                               u32 columns,
                               u32 rows) {
  zero(term, terminal);
  if (!allocate_byte_buffer(&term->buffer, buffer_size)) {
    log_error("could not allocate working buffer");
    return 0;
  }
  if (!allocate_scrollback_buffer(&term->scrollback, scrollback_buffer_size, max_lines)) {
    log_error("could not allocate scrollback buffer");
    return 0;
  }

  for (auto i = 1; i < MAX_ROWS; i++) {
    term->row_data[i].line = INVALID_ROW_INDEX;
  }

  term->program = program;
  term->cursor.x = 0;
  term->cursor.y = 0;
  term->columns = columns;
  term->rows = rows;
  term->size = size;
  term->character_cell_scratch_buffer.cells = term->character_cell_scratch;
  if (create_pty(term)) {
    update_terminal_size(term);
    return 0;
  }
  return 1;
}


#define INVALID_UTF8 -1

#define utf_cont(ch)  (((ch) & 0xc0) == 0x80)

// TODO: Make stateful
internal size_t utf8iterate(const u8 *str,
                            size_t strlen,
                            rune *dst) {
  u32 uc;
  const u8 *end;

  *dst = -1;
  if (!strlen) return 0;
  end = str + ((strlen < 0) ? 4 : strlen);
  uc = *str++;
  if (uc < 0x80) {
    *dst = uc;
    return 1;
  }
  // Must be between 0xc2 and 0xf4 inclusive to be valid
  if ((u32)(uc - 0xc2) > (0xf4-0xc2)) return INVALID_UTF8;
  if (uc < 0xe0) {         // 2-byte sequence
    // Must have valid continuation character
    if (str >= end || !utf_cont(*str)) return INVALID_UTF8;
    *dst = ((uc & 0x1f)<<6) | (*str & 0x3f);
    return 2;
  }
  if (uc < 0xf0) {        // 3-byte sequence
    if ((str + 1 >= end) || !utf_cont(*str) || !utf_cont(str[1]))
      return INVALID_UTF8;
    // Check for surrogate chars
    if (uc == 0xed && *str > 0x9f)
      return INVALID_UTF8;
    uc = ((uc & 0xf)<<12) | ((*str & 0x3f)<<6) | (str[1] & 0x3f);
    if (uc < 0x800)
      return INVALID_UTF8;
    *dst = uc;
    return 3;
  }
  // 4-byte sequence
  // Must have 3 valid continuation characters
  if ((str + 2 >= end) || !utf_cont(*str) || !utf_cont(str[1]) || !utf_cont(str[2]))
    return INVALID_UTF8;
  // Make sure in correct range (0x10000 - 0x10ffff)
  if (uc == 0xf0) {
    if (*str < 0x90) return INVALID_UTF8;
  } else if (uc == 0xf4) {
    if (*str > 0x8f) return INVALID_UTF8;
  }
  *dst = ((uc & 7)<<18) | ((*str & 0x3f)<<12) | ((str[1] & 0x3f)<<6) | (str[2] & 0x3f);
  return 4;
}

interenal b32 cursor_to_insert_point(terminal *term, u32 *line_index, u32 *start) {
  row_data *rd = &term->row_data[term->cursor.y];
  if (rd->line != INVALID_ROW_INDEX) {
    if (rd->line == term->scrollback_buffer.bottom_line) {
      if (term->cursor.x == term->scrollback_buffer.lines[rd->line].count - 1) {
        *line_index = rd->line;
        *start = term->cursor.x;
        return 0; // append
      }
    } else {
      if (term->cursor.x < term->scrollback_buffer.lines[rd->line].count) {
        line_index = rd->line;
        start = term->cursor.x;
        return 1; // splice
      } else {
        // We need to "open up space"
        splice_into_line(&term->scrollback_buffer, rd->line, start, &term->character_cell_scratch_buffer);
      }
    }
  }
}

// This function has not error checking of any kind.  Beware!
internal u32 append_rune_to_scratch(terminal *term, rune r) {
  u32 index = term->character_cell_scratch_buffer.count++;
  term->character_cell_scratch[index].c.rune = r;
  term->character_cell_scratch[index].foreground = term->current_foreground_color;
  term->character_cell_scratch[index].background = term->current_background_color;
  term->character_cell_scratch[index].flags = term->current_character_attribute;
  return index + 1;
}

// This function has not error checking of any kind.  Beware!
internal u32 append_byte_to_scratch(terminal *term, u8 byte) {
  u32 index = term->character_cell_scratch_buffer.count++;
  term->character_cell_scratch[index].c.byte = byte;
  term->character_cell_scratch[index].foreground = term->current_foreground_color;
  term->character_cell_scratch[index].background = term->current_background_color;
  term->character_cell_scratch[index].flags = term->current_character_attribute;
  return index + 1;
}

// TODO: All of the "write" routines are incomplete.  They do not process
//       end of line events at all.  I need to think about this situation some more

internal void write_rune(terminal *term, rune r) {
  u32 line_index = 0, start = 0;
  term->character_cell_scratch_buffer.count = 0;
  append_rune_to_scratch(term, r);
  if (cursor_to_insert_point(term, &line_index, &start)) {
    splice_into_line(&term->scrollback_buffer, line_index, start, &term->character_cell_scratch_buffer);
  } else {
    append_cells(&term->scrollback_buffer, &term->character_cell_scratch_buffer, 0);
  }
}

internal void write_byte(terminal *term, u8 byte) {
  u32 line_index = 0, start = 0;
  term->character_cell_scratch_buffer.count = 0;
  append_byte_to_scratch(term, byte);
  if (cursor_to_insert_point(term, &line_index, &start)) {
    splice_into_line(&term->scrollback_buffer, line_index, start, &term->character_cell_scratch_buffer);
  } else {
    append_cells(&term->scrollback_buffer, &term->character_cell_scratch_buffer, 0);
  }
}

internal void write_string(terminal *term, char *s) {
  u32 line_index = 0, start = 0, index = 0;
  term->character_cell_scratch_buffer.count = 0;
  for (char c = *s; c != 0; c = *++s) {
    if (index == CHARACTER_CELL_SCRATCH_SIZE) {
      if (cursor_to_insert_point(term, &line_index, &start)) {
        splice_into_line(&term->scrollback_buffer, line_index, start, &term->character_cell_scratch_buffer);
      } else {
        append_cells(&term->scrollback_buffer, &term->character_cell_scratch_buffer, 0);
      }
      term->character_cell_scratch_buffer.count = 0;
    }
    index = append_byte_to_scratch(term, s);
  }
  if (cursor_to_insert_point(term, &line_index, &start)) {
    splice_into_line(&term->scrollback_buffer, line_index, start, &term->character_cell_scratch_buffer);
  } else {
    append_cells(&term->scrollback_buffer, &term->character_cell_scratch_buffer, 0);
  }
}

internal void write_nstring(terminal *term, char *s, u32 string_length) {
  u32 line_index = 0, start = 0, index = 0;
  term->character_cell_scratch_buffer.count = 0;
  char c = *s;
  for (auto rem = string_length; rem > 0; rem--) {
    if (index == CHARACTER_CELL_SCRATCH_SIZE) {
      if (cursor_to_insert_point(term, &line_index, &start)) {
        splice_into_line(&term->scrollback_buffer, line_index, start, &term->character_cell_scratch_buffer);
      } else {
        append_cells(&term->scrollback_buffer, &term->character_cell_scratch_buffer, 0);
      }
      term->character_cell_scratch_buffer.count = 0;
    }
    index = append_byte_to_scratch(term, s);
    c = *++s;
  }
  if (cursor_to_insert_point(term, &line_index, &start)) {
    splice_into_line(&term->scrollback_buffer, line_index, start, &term->character_cell_scratch_buffer);
  } else {
    append_cells(&term->scrollback_buffer, &term->character_cell_scratch_buffer, 0);
  }
}

internal void reset_character_attributes(terminal *term) {
  term->current_character_attributes = 0;
  term->current_foreground_color = term->default_foreground_color;
  term->current_background_color = term->default_background_color;
}

internal void set_char_attribute(terminal *term, u32 char_flags) {
  term->current_character_attributes |= char_flags;
}

internal void reset_char_attribute(terminal *term, u32 char_flags) {
  term->current_character_attributes &= char_flags;
}

internal void set_font(terminal *term, u32 font_index) {

}

internal void set_default_foreground_color(terminal *term) {
  term->current_foreground_color = term->default_foreground_color;
}

internal void set_default_background_color(terminal *term) {
  term->current_background_color = term->default_background_color;
}

internal void set_foreground_color(terminal *term, u32 color) {
  term->current_foreground_color = color;
}

internal void set_background_color(terminal *term, u32 color) {
  term->current_background_color = color;
}

internal void erase_screen(terminal *term, u32 erase) {
  switch erase {
  case 0: // cursor to end of screen
  case 1: // cursor to beginning of screen
  case 2: // clear entire screen
  case 3: // clear screen and backbuffer
  }
}

internal void erase_line(terminal *term, u32 erase) {

}

internal void move_relative(terminal *term, s32 dx, s32 dy) {
  u32 x = term->cursor.x;
  u32 y = term->cursor.y;
  if (dx < 0) {
    if (dx < x) {
      x += dx;
    } else {
      x = 0;
    }
  } if (dx > 0) {
    if (dx + x < term->columns) {
      x += dx
    } else {
      x = term->columns - 1;
    }
  }
  if (dy < 0) {
    if (dy < y) {
      y += dy;
    } else {
      y = 0;
    }
  } if (dy > 0) {
    if (dy + y < term->rows) {
      y += dy
    } else {
      y = term->rows - 1;
    }
  }

  term->cursor.x = x;
  term->cursor.y = y;
}

internal void move_horizontal_absolute(terminal *term, u32 x) {
  if (x < term->columns) {
    term->cursor.x = x;
  } else {
    term->cursor.x = term.columns-1;
  }
}

internal void move_absolute(terminal *term, u32 x, u32 y) {
  if (x < term->columns) {
    term->cursor.x = x;
  } else {
    term->cursor.x = term.columns-1;
  }

  if (y < term->rows) {
    term->cursor.y = y;
  } else {
    term->cursor.y = term->rows-1;
  }
}


internal void parse_data(terminal *term) {
  buffer buf;
  get_next_readable_range(&term->buffer, &buf);
  auto end = buf.data + buf.count;
  auto remaining = buf.count;
  for (auto addr = buf.data; addr < end;) {
    switch (term->parse_state) {
    case PS_NORMAL:
      if (*addr == escape) {
        clear_accumulator(term);
        term->parse_state = PS_VT_CODE;
        reset_escape_parser_state(&term->escape_parser);
        addr++;
      } else {
        rune r = 0;
        auto bytes = utf8iterate(addr, remaining, &r);
        if (bytes != INVALID_UTF8) {
          write_byte(term, *addr);
          addr++;
          remaining--;
        } else {
          write_rune(term, r);
          addr += bytes;
          remaining -= bytes;
        }
      }
      break;
    case PS_VT_CODE:
      switch (term->escape_parser.state) {
      case ESP_READY:
      case ESP_INCOMPLETE:
        switch (feed_parser(&term->escape_parser, *addr)) {
        case ESP_ERROR:
          write_string(term, "^[");
          write_nstring(term, term->escape_parser.buffer, term->escape_parser.length);
          term->parse_state = PS_NORMAL;
          break;
        case ESP_COMPLETE:
          switch (term->escape_parser.result_type) {
          case ESPR_INVALID:
            write_string(term, "^[");
            write_nstring(term, term->escape_parser.buffer, term->escape_parser.length);
            break;
          case ESPR_CHAR_ATTRIBUTE_RESET_ALL:
            reset_char_attributes(term);
            break;
          case ESPR_CHAR_ATTRIBUTE_SET:
            set_char_attribute(term, term->escape_parser.char_flags);
            break;
          case ESPR_CHAR_ATTRIBUTE_RESET:
            reset_char_attribute(term, term->escape_parser.char_flags);
            break;
          case ESPR_FONT:
            set_font(term, term->escape_parser.font_index);
            break;
          case ESPR_DEFAULT_FOREGROUND_COLOR:
            set_default_foreground_color(term);
            break;
          case ESPR_DEFAULT_BACKGROUND_COLOR:
            set_default_background_color(term);
            break;
          case ESPR_FOREGROUND_16_BIT_COLOR:
          case ESPR_FOREGROUND_256_BIT_COLOR:
          case ESPR_FOREGROUND_TRUE_COLOR:
            set_foreground_color(term, term->escape_parser.color.packed);
            break;
          case ESPR_BACKGROUND_256_BIT_COLOR:
          case ESPR_BACKGROUND_16_BIT_COLOR:
          case ESPR_BACKGROUND_TRUE_COLOR:
            set_background_color(term, term->escape_parser.color.packed);
            break;
          case ESPR_ERASE_SCREEN:
            erase_screen(term, term->escape_parser.erase);
            break;
          case ESPR_ERASE_LINE:
            erase_line(term, term->escape_parser.erase);
            break;
          case ESPR_MOVE_RELATIVE:
            move_relative(term, term->escape_parser.relative.dx, term->escape_parser.relative.dy);
            break;
          case ESPR_MOVE_HORIZONTAL_ABSOLUTE:
            move_horizontal_absolute(term, term->escape_parser.absolute.x);
            break;
          case ESPR_MOVE_ABSOLUTE:
            move_absolute(term, term->escape_parser.x, term->escape_parser.y);
            break;
          }
          term->parse_state = PS_NORMAL;
          break;
        default:
          // nothing
        }
      }
      break;
    case PS_UNICODE:
      break;
    }
  }

}
