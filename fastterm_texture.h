
#if !defined(FASTTERM_TEXTURE)
#define FASTTERM_TEXTURE

#include "fastterm_glyphcache.cpp"

typedef enum render_mode {
  NORMAL,
  LIGHT,
  LCD
} render_mode;

typedef struct glyph_gen {
  FT_Render_Mode  render_mode;
  s32             load_flags;
  s32             load_target;
  u32             bytes_per_pixel;
  FT_F26Dot6      char_width;
  FT_F26Dot6      char_height;
  u32             x_dpi;
  u32             y_dpi;
} glyph_gen;

typedef struct render_mode_result {
  FT_Render_Mode   mode;
  s32              load_flags;
} render_mode_result;

typedef struct max_pixel_dimensions {
  u32 max_width, max_height;
} max_pixel_dimensions;

typedef struct glyph_texture {
  u8          *buffer;
  u32          buffer_size;
  u32          texture_row_bytes_width;
  u32          texture_pixel_width;
  u32          texture_pixel_height;
  u32          glyph_byte_width;
  u32          glyph_pixel_width;
  u32          glyph_pixel_height;
  u32          half_glyph_pixel_width;
  u32          ascender_pixels;
  FT_Face      face;
  glyph_gen    generator;
  void        *glyph_cache_memory;
  glyph_table *glyph_table;
  f64          point_size;
} glyph_texture;


#endif
