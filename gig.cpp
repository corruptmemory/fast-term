#include <stdio.h>
#include <string.h>

const char* const s = "12345678901234567890123456789012345678901234567890123456789012345678901234567890";

int main(int argc, char **argv) {
  const int l = strlen(s);
  const int c = (1024*1024*1024)/l;
  for (auto i = 0; i < c; i++) {
    puts(s);
  }
}
