#define LOG_DEBUG

#include "../fastterm_types.h"
#include "../fastterm_logging.cpp"
#include "../fastterm_bytebuffer.cpp"
#include <stdlib.h>
#include <time.h>

const char *const s = "This is some sample data";

internal void basic_ops() {
  log_info("Testing basic ops");
  byte_buffer bb;
  if (!allocate_byte_buffer(&bb, 1024)) {
    log_error("Failed to allocate byte buffer");
    return;
  }

  buffer rrange;
  get_next_readable_range(&bb, &rrange);
  assert(rrange.count == 0);
  buffer wrange;
  get_next_writable_range(&bb, &wrange, MAX_WRITEABLE);
  assert(wrange.count == bb.data_size);
  
  buffer sb = {
    .count = (u32) strlen(s),
    .data = (u8*) s
  }; 
  write_data(&wrange, &sb);
  commit_write(&bb, sb.count);
  get_next_readable_range(&bb, &rrange);
  assert(rrange.count == sb.count);
  assert(memcmp(rrange.data, sb.data, sb.count) == 0);
  get_next_writable_range(&bb, &wrange, MAX_WRITEABLE);  
  assert(wrange.count == bb.data_size - sb.count);
  commit_read(&bb, rrange.count);
  get_next_readable_range(&bb, &rrange);
  assert(rrange.count == 0);
  get_next_writable_range(&bb, &wrange, MAX_WRITEABLE);
  assert(wrange.count == bb.data_size);
  delete_byte_buffer(&bb);
}

internal void fill_up() {
  log_info("Testing complete fill");
  byte_buffer bb;
  if (!allocate_byte_buffer(&bb, 1024)) {
    log_error("Failed to allocate byte buffer");
    return;
  }

  buffer wrange;
  buffer sb = {
    .count = (u32) strlen(s),
    .data = (u8*) s
  }; 

  for (get_next_writable_range(&bb, &wrange, MAX_WRITEABLE);
       wrange.count > 0;
       get_next_writable_range(&bb, &wrange, MAX_WRITEABLE)) {
    auto l = write_data(&wrange, &sb);
    commit_write(&bb, l);
  }
  buffer rrange;
  get_next_readable_range(&bb, &rrange);
  assert(rrange.count == bb.data_size);
  delete_byte_buffer(&bb);
}

internal void fill_up_and_drain() {
  log_info("Testing complete fill then complete drain");
  byte_buffer bb;
  if (!allocate_byte_buffer(&bb, 1024)) {
    log_error("Failed to allocate byte buffer");
    return;
  }

  buffer wrange;
  buffer sb = {
    .count = (u32) strlen(s),
    .data = (u8*) s
  }; 

  for (get_next_writable_range(&bb, &wrange, MAX_WRITEABLE);
       wrange.count > 0;
       get_next_writable_range(&bb, &wrange, MAX_WRITEABLE)) {
    auto l = write_data(&wrange, &sb);
    commit_write(&bb, l);
  }
  buffer rrange;
  get_next_readable_range(&bb, &rrange);
  assert(rrange.count == bb.data_size);
  commit_read(&bb, rrange.count);
  get_next_readable_range(&bb, &rrange);
  assert(rrange.count == 0);
  get_next_writable_range(&bb, &wrange, MAX_WRITEABLE);
  assert(wrange.count == bb.data_size);
  delete_byte_buffer(&bb);
}

internal void fill_with_pattern(buffer *bytes, u8 skip = 1) {
  u8 c = 0;
  for (u32 i = 0; i < bytes->count; i++) {
    bytes->data[i] = c;
    c += skip;
  }
}

internal b32 confirm_pattern(buffer *bytes, u8 skip = 1, u8 start = 0) {
  u8 c = start;
  for (u32 i = 0; i < bytes->count; i++) {
    if (bytes->data[i] != c) {
      log_error("Error: mismatch at %d; expected: %d found %d", i, c, bytes->data[i]);
      return 0;
    }
    c += skip;
  }
  return 1;
}

internal void use_hardware_wrap() {
  log_info("Testing hardware wrapping");
  byte_buffer bb;
  if (!allocate_byte_buffer(&bb, 1024)) {
    log_error("Failed to allocate byte buffer");
    return;
  }

  buffer wrange;
  buffer rrange;

  // Fill to half way
  get_next_writable_range(&bb, &wrange, bb.data_size/2);
  assert(wrange.count == bb.data_size/2);
  fill_with_pattern(&wrange, 2);
  commit_write(&bb, wrange.count);
  
  // Read all the stuff 
  get_next_readable_range(&bb, &rrange);
  assert(rrange.count == bb.data_size/2);
  if (!confirm_pattern(&rrange, 2)) {
    log_error("Failed to find expected pattern of bytes");
    return;
  }
  commit_read(&bb, rrange.count);

  // Now that we are half-way in we should try to write the whole thing
  get_next_writable_range(&bb, &wrange, MAX_WRITEABLE);
  assert(wrange.count == bb.data_size);
  fill_with_pattern(&wrange, 1);
  commit_write(&bb, wrange.count);

  // Read all the stuff -- This should be a linear buffer but actually wrap 
  get_next_readable_range(&bb, &rrange);
  assert(rrange.count == bb.data_size);
  if (!confirm_pattern(&rrange, 1)) {
    log_error("Failed to find expected pattern of bytes");
    return;
  }
  // We should see a logical continuation of the pattern at the front of the
  // data array
  buffer sb = {
    .count = rrange.count/2,
    .data = bb.data
  };
  if (!confirm_pattern(&sb, 1, (u8)((bb.start + rrange.count/2)%256))) {
    log_error("Could not see expected wrap-around bytes");
    return;
  }
  
  commit_read(&bb, rrange.count);

  get_next_readable_range(&bb, &rrange);
  assert(rrange.count == 0);
  get_next_writable_range(&bb, &wrange, MAX_WRITEABLE);
  assert(wrange.count == bb.data_size);
  delete_byte_buffer(&bb);
}

internal u32 u32_max(u32 m) {
  return (u32) rand() % m;
}

internal void randomized_workload() {
  log_info("Testing randomized workload");
  byte_buffer bb;
  if (!allocate_byte_buffer(&bb, 1024)) {
    log_error("Failed to allocate byte buffer");
    return;
  }

  buffer wrange;
  buffer rrange;

  srand(time(NULL));
  u32 a_bit_over = (u32)((f32)(bb.data_size) * 1.10);
  log_info("... lockstep");
  for (auto i = 0; i < 100000; i++) {
    u32 request = u32_max(a_bit_over) + 10;
    u8 skip = (u8) u32_max(5);
    if (request > bb.data_size) {
      request = MAX_WRITEABLE;
    }
    get_next_writable_range(&bb, &wrange, request);
    fill_with_pattern(&wrange, skip);
    commit_write(&bb, wrange.count);
    get_next_readable_range(&bb, &rrange);
    if (!confirm_pattern(&rrange, skip)) {
      log_error("Failed to confirm expected pattern");
      return;
    }
    commit_read(&bb, rrange.count);
  }
  log_info("... async");
  u32 writes = 0;
  u32 reads = 0;
  u64 total_written = 0;
  u64 total_read = 0;
  for (auto i = 0; i < 1000000; i++) {
    if (u32_max(3) > 1) {
      writes++;
      u32 request = u32_max(a_bit_over) + 10;
      if (request > bb.data_size) {
        request = MAX_WRITEABLE;
      }
      get_next_writable_range(&bb, &wrange, request);
      if (wrange.count > 0) {
        total_written += wrange.count;
        u8 skip = (u8) u32_max(5);
        fill_with_pattern(&wrange, skip);
        commit_write(&bb, wrange.count);
      }
    } else {
      reads++;
      get_next_readable_range(&bb, &rrange);
      if (rrange.count > 0) {
        u32 some_valid_amount = u32_max(rrange.count);
        if (some_valid_amount == 0) {
          some_valid_amount = u32((f32)(rrange.count) * 0.1);
        }
        commit_read(&bb, rrange.count);
        total_read += some_valid_amount;
      }
    }
  }
  log_info("Writes: %d", writes);
  log_info("Total written: %d", total_written);
  log_info("Reads: %d", reads);
  log_info("Total read: %d", total_read);
  delete_byte_buffer(&bb);
}


int main(int argc, char **argv) {
  basic_ops();
  fill_up();
  fill_up_and_drain();
  use_hardware_wrap();
  randomized_workload();
}
