#define LOG_DEBUG

#include "../fastterm_types.h"
#include "../fastterm_logging.cpp"
#include "../fastterm_escape_code_parser.cpp"
#include <string.h>

typedef struct test_case {
  const char          *input;
  escape_parser_state  result;
} test_case;

u32 test_index_colors[16] = {
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
};

test_case test_cases[] = {
  {
    .input = "[m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_CHAR_ATTRIBUTE_RESET_ALL
    }
  },
  {
    .input = "[0m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_CHAR_ATTRIBUTE_RESET_ALL
    }
  },
  {
    .input = "[10A",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_RELATIVE,
      .relative = { 0, -10 }
    }
  },
  {
    .input = "[10B",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_RELATIVE,
      .relative = { 0, 10 }
    }
  },
  {
    .input = "[10C",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_RELATIVE,
      .relative = { 10, 0 }
    }
  },
  {
    .input = "[10D",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_RELATIVE,
      .relative = { -10, 0 }
    }
  },
  {
    .input = "[10E",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_RELATIVE,
      .relative = { 0, 10 }
    }
  },
  {
    .input = "[10F",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_RELATIVE,
      .relative = { 0, -10 }
    }
  },
  {
    .input = "[10G",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_HORIZONTAL_ABSOLUTE,
      .absolute = { 10, 0 }
    }
  },
  {
    .input = "[J",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_ERASE_SCREEN,
    }
  },
  {
    .input = "[0J",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_ERASE_SCREEN,
    }
  },
  {
    .input = "[1J",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_ERASE_SCREEN,
      .erase = 1
    }
  },
  {
    .input = "[2J",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_ERASE_SCREEN,
      .erase = 2
    }
  },
  {
    .input = "[3J",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_ERASE_SCREEN,
      .erase = 3
    }
  },
  {
    .input = "[K",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_ERASE_LINE,
    }
  },
  {
    .input = "[0K",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_ERASE_LINE,
    }
  },
  {
    .input = "[1K",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_ERASE_LINE,
      .erase = 1
    }
  },
  {
    .input = "[2K",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_ERASE_LINE,
      .erase = 2
    }
  },
  {
    .input = "[;H",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_ABSOLUTE,
      .absolute = { 1, 1 }
    }
  },
  {
    .input = "[12;H",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_ABSOLUTE,
      .absolute = { 1, 12 }
    }
  },
  {
    .input = "[;15H",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_ABSOLUTE,
      .absolute = { 15, 1 }
    }
  },
  {
    .input = "[12;15H",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_ABSOLUTE,
      .absolute = { 15, 12 }
    }
  },
  {
    .input = "[;f",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_ABSOLUTE,
      .absolute = { 1, 1 }
    }
  },
  {
    .input = "[12;f",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_ABSOLUTE,
      .absolute = { 1, 12 }
    }
  },
  {
    .input = "[;15f",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_ABSOLUTE,
      .absolute = { 15, 1 }
    }
  },
  {
    .input = "[12;15f",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_MOVE_ABSOLUTE,
      .absolute = { 15, 12 }
    }
  },
  {
    .input = "[10m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_FONT,
      .font_index = 0
    }
  },
  {
    .input = "[19m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_FONT,
      .font_index = 9
    }
  },
  {
    .input = "[30m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_FOREGROUND_16_BIT_COLOR,
      .color = {
        .packed = test_index_colors[0]
      }
    }
  },
  {
    .input = "[37m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_FOREGROUND_16_BIT_COLOR,
      .color = {
        .packed = test_index_colors[7]
      }
    }
  },
  {
    .input = "[40m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_BACKGROUND_16_BIT_COLOR,
      .color = {
        .packed = test_index_colors[0]
      }
    }
  },
  {
    .input = "[47m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_BACKGROUND_16_BIT_COLOR,
      .color = {
        .packed = test_index_colors[7]
      }
    }
  },
  {
    .input = "[90m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_FOREGROUND_16_BIT_COLOR,
      .color = {
        .packed = test_index_colors[8]
      }
    }
  },
  {
    .input = "[97m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_FOREGROUND_16_BIT_COLOR,
      .color = {
        .packed = test_index_colors[15]
      }
    }
  },
  {
    .input = "[100m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_BACKGROUND_16_BIT_COLOR,
      .color = {
        .packed = test_index_colors[8]
      }
    }
  },
  {
    .input = "[107m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_BACKGROUND_16_BIT_COLOR,
      .color = {
        .packed = test_index_colors[15]
      }
    }
  },
  {
    .input = "[39m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_DEFAULT_FOREGROUND_COLOR,
    }
  },
  {
    .input = "[49m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_DEFAULT_BACKGROUND_COLOR,
    }
  },
  {
    .input = "[1m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_CHAR_ATTRIBUTE_SET,
      .char_flags = CF_BOLD
    }
  },
  {
    .input = "[4m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_CHAR_ATTRIBUTE_SET,
      .char_flags = CF_UNDERLINE
    }
  },
  {
    .input = "[22m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_CHAR_ATTRIBUTE_RESET,
      .char_flags = (u32) ~CF_BOLD
    }
  },
  {
    .input = "[24m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_CHAR_ATTRIBUTE_RESET,
      .char_flags = (u32) ~CF_UNDERLINE
    }
  },
  {
    .input = "[38;5;0m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_FOREGROUND_256_BIT_COLOR,
      .color = {
        .packed = color_cube[0]
      }
    }
  },
  {
    .input = "[38;5;244m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_FOREGROUND_256_BIT_COLOR,
      .color = {
        .packed = color_cube[244]
      }
    }
  },
  {
    .input = "[38;2;0;0;0m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_FOREGROUND_TRUE_COLOR,
      .color = {
        .rgb = { .b = 0, .g = 0, .r = 0 }
      }
    }
  },
  {
    .input = "[38;2;100;50;25m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_FOREGROUND_TRUE_COLOR,
      .color = {
        .rgb = { .b = 25, .g = 50, .r = 100 }
      }
    }
  },
  {
    .input = "[48;2;0;0;0m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_BACKGROUND_TRUE_COLOR,
      .color = {
        .rgb = { .b = 0, .g = 0, .r = 0 }
      }
    }
  },
  {
    .input = "[48;2;128;64;255m",
    .result = {
      .state = ESP_COMPLETE,
      .result_type = ESPR_BACKGROUND_TRUE_COLOR,
      .color = {
        .rgb = { .b = 255, .g = 64, .r = 128 }
      }
    }
  },
  {
    .input = NULL
  }
};

internal b32 check_function(test_case *test) {
  escape_parser_state s;
  reset_escape_parser_state(&s);
  s.index_colors = test_index_colors;

  auto il = strlen(test->input);
  for (unsigned long i = 0; i < il; i++) {
    auto r = feed_parser(&s, test->input[i]);
    if (r == ESP_COMPLETE) {
      break;
    } else if (r == ESP_ERROR) {
      log_error("Got parser error for: %s -> %s", test->input, s.buffer);
      return 0;
    }
  }
  
  if (strcmp(test->input + 1, (const char *)s.buffer) != 0) {
    log_error("Error: parser didn't parse input correctly.  Input: %s, Result: %s", test->input, s.buffer);
    return 0;
  }

  if (test->result.state != s.state) {
    log_error("Error: parser result state invalid %d != %d", test->result.state, s.state);
    return 0;
  }

  if (test->result.result_type != s.result_type) {
    log_error("Error: parser result type invalid %d != %d", test->result.result_type, s.result_type);
    return 0;
  }

  switch (s.result_type) {
  case ESPR_INVALID:
    log_error("Error: result type invalid: %s", s.buffer);
    return 0;
  case ESPR_CHAR_ATTRIBUTE_SET:
    if (test->result.char_flags != s.char_flags) {
      log_error("Error: parser char flags mismatch %d != %d", test->result.char_flags, s.char_flags);
      return 0;
    }
    break;
  case ESPR_CHAR_ATTRIBUTE_RESET:
    if (test->result.char_flags != s.char_flags) {
      log_error("Error: parser char flags mismatch %d != %d", test->result.char_flags, s.char_flags);
      return 0;
    }
    break;
  case ESPR_CHAR_ATTRIBUTE_RESET_ALL:
    break;
  case ESPR_FONT:
    if (test->result.font_index != s.font_index) {
      log_error("Error: parser font index mismatch %d != %d", test->result.font_index, s.font_index);
      return 0;
    }
    break;
  case ESPR_DEFAULT_FOREGROUND_COLOR:
  case ESPR_DEFAULT_BACKGROUND_COLOR:
  case ESPR_FOREGROUND_16_BIT_COLOR:
  case ESPR_BACKGROUND_16_BIT_COLOR:
  case ESPR_FOREGROUND_256_BIT_COLOR:
  case ESPR_BACKGROUND_256_BIT_COLOR:
  case ESPR_FOREGROUND_TRUE_COLOR:
  case ESPR_BACKGROUND_TRUE_COLOR:
    if (test->result.color.packed != s.color.packed) {
      log_error("Error: parser color.packed mismatch %x != %x", test->result.color.packed, s.color.packed);
      return 0;
    }
    break;
  case ESPR_ERASE_SCREEN:
  case ESPR_ERASE_LINE:
    if (test->result.erase != s.erase) {
      log_error("Error: parser erase mismatch %d != %d", test->result.erase, s.erase);
      return 0;
    }
    break;
  case ESPR_MOVE_RELATIVE:
    if (test->result.relative.dx != s.relative.dx || test->result.relative.dy != s.relative.dy) {
      log_error("Error: parser relative move missmatch (%d, %d) != (%d, %d)", test->result.relative.dx, test->result.relative.dy, s.relative.dx, s.relative.dy);
      return 0;
    }
    break;
  case ESPR_MOVE_HORIZONTAL_ABSOLUTE:
  case ESPR_MOVE_ABSOLUTE:
    if (test->result.absolute.x != s.absolute.x || test->result.absolute.y != s.absolute.y) {
      log_error("Error: parser absolute move missmatch (%d, %d) != (%d, %d)", test->result.absolute.x, test->result.absolute.y, s.absolute.x, s.absolute.y);
      return 0;
    }
    break;
  }
  
  return 1;
}

internal void basic_ops() {
  log_info("Testing basic ops");
  for (test_case *t = test_cases; t->input != NULL; t++) {
    if (!check_function(t)) {
      return;
    }
  }
}


int main(int argc, char **argv) {
  basic_ops();
}
