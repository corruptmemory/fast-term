#define LOG_DEBUG
#define DEBUG_LRU

#include "../fastterm_types.h"
#include "../fastterm_logging.cpp"
#include "../fastterm_glyphcache.cpp"
#include <stdlib.h>
#include <time.h>
#include <string.h>

#if 0
internal void print_stats(glyph_table_stats *stats) {
  printf("== Stats:\n");
  printf("   hit_count: %d\n", stats->hit_count);
  printf("   miss_count: %d\n", stats->miss_count);
  printf("   recycle_count: %d\n", stats->recycle_count);
}

internal void print_hash_table(u32 *table, u32 count) {
  printf("== Hash table:\n");
  printf("   hash_table_count: %d\n", count);
  printf("   === Entries ===");
  auto append = 0;
  for (auto i = 0; i < count; i++) {
    if (append) printf(", ");
    if (i % 50 == 0) {
      printf("\n   ");
    }
    printf("%d", table[i]);
    append = 1;
  }
}

internal void print_entry(glyph_entry *entry) {
  printf("== Entry:\n");
  printf("   hash_value:          %d\n", entry->hash_value);
  printf("   next_with_same_hash: %d\n", entry->next_with_same_hash);
  printf("   next_lru:            %d\n", entry->next_lru);
  printf("   prev_lru:            %d\n", entry->prev_lru);
  printf("   gpu_index:           %d\n", entry->gpu_index);
  printf("   filled_state:        %d\n", entry->filled_state);
  printf("   dim_x:               %d\n", entry->dim_x);
  printf("   dim_y:               %d\n", entry->dim_y);
#ifdef DEBUG_LRU
  printf("   ordering:            %d\n", entry->ordering);
#endif

}

internal void print_table(glyph_table *table) {
  printf("===== Glyph Table =====\n");
  print_stats(&table->stats);
  printf("hash_mask: %08X\n", table->hash_mask);
#ifdef DEBUG_LRU
  printf("last_lru_count: %d\n", table->last_lru_count);
#endif
  print_hash_table(table->hash_table, table->hash_table_count);
  printf("\nentry_count: %d\n", table->entry_count);
  printf("== Entries ==\n");
  for (u32 i = 0; i < table->entry_count; i++) {
    print_entry(table->entries + i);
  }
}
#endif

internal void basic_ops() {
  log_info("Testing basic ops");
  glyph_table_params params = {
    .hash_count = 4096,
    .entry_count = 1024,
    .reserved_tile_count = 0,
    .cache_tile_count_in_x = 64
  };
  auto size = get_glyph_table_footprint(&params);
  void *mem = calloc(size, 1);
  auto table = place_glyph_table_in_memory(&params, mem);
  for (auto i = 33; i < 127; i++) {
    auto hash = rune_hash(i);
    glyph_state state;
    find_glyph_entry_by_hash(table, hash, &state);
    assert(state.filled_state == EMPTY);
    assert(state.dim_x == 0);
    assert(state.dim_y == 0);
    update_glyph_cache_entry(table, state.id, FILLED, 1, 1);
    find_glyph_entry_by_hash(table, hash, &state);
    assert(state.filled_state == FILLED);
    assert(state.dim_x == 1);
    assert(state.dim_y == 1);
  }
  assert(table->stats.recycle_count == 0);
  free(mem);
}

internal void overcommit() {
  log_info("Testing overcommit");
  glyph_table_params params = {
    .hash_count = 4096,
    .entry_count = 1024,
    .reserved_tile_count = 0,
    .cache_tile_count_in_x = 64
  };
  auto size = get_glyph_table_footprint(&params);
  void *mem = calloc(size, 1);
  auto table = place_glyph_table_in_memory(&params, mem);
  for (auto i = 0; i < 10000; i++) {
    auto hash = rune_hash(i);
    glyph_state state;
    find_glyph_entry_by_hash(table, hash, &state);
    assert(state.filled_state == EMPTY);
    assert(state.dim_x == 0);
    assert(state.dim_y == 0);
    update_glyph_cache_entry(table, state.id, FILLED, 1, 1);
    find_glyph_entry_by_hash(table, hash, &state);
    assert(state.filled_state == FILLED);
    assert(state.dim_x == 1);
    assert(state.dim_y == 1);
  }
  assert(table->stats.recycle_count > 0);
  free(mem);
}


int main(int argc, char **argv) {
  basic_ops();
  overcommit();
}
