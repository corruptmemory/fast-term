#define LOG_DEBUG
#define DEBUG_LRU

#include "../fastterm_types.h"
#include "../fastterm_logging.cpp"
#include "../fastterm_glyph_texture.cpp"
#include "../fastterm_stb_implementation.cpp"
#include <stdlib.h>
#include <time.h>
#include <string.h>

internal FT_Face load_test_font(FT_Library freetype) {
  FT_Face face;
  FT_Error fterr = FT_New_Face(freetype, "/usr/share/fonts/source-code-pro/SourceCodePro-Regular.ttf", 0, &face);
  if (fterr) {
    log_error("Failed to load test face: %d", fterr);
    exit(-1);
  }
  return face;
}
  
internal void basic_ops() {
  log_info("Testing basic ops");
  FT_Library freetype;
  FT_Error fterr = FT_Init_FreeType(&freetype);
  if (fterr) {
    log_error("Failed to load FreeType: %d", fterr);
    exit(-1);
  };
  FT_Face face = load_test_font(freetype);
  glyph_texture texture;
  if (!init_glyph_texture(&texture,
                          face,
                          16.0,
                          92,
                          92)) {
    log_error("Failed to initialize texture data structure");
    exit(-1);
  }

  glyph_texture_write_to_file(&texture, "pixels.png");
  
  destroy_glyph_texture(&texture);
  FT_Done_FreeType(freetype);
}

int main(int argc, char **argv) {
  basic_ops();
}
