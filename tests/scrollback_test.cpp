#define LOG_DEBUG

#include "../fastterm_types.h"
#include "../fastterm_logging.cpp"
#include "../fastterm_scrollback.cpp"
#include <stdlib.h>
#include <time.h>
#include <string.h>

#if 0
internal void print_cell(character_cell *c, const char *const padding = "") {
  printf("%sc:          %d\n", padding, c->c.rune);
  printf("%sforeground: %d\n", padding, c->foreground);
  printf("%sbackground: %d\n", padding, c->background);
  printf("%sflags:      %d\n", padding, c->flags);
}
#endif

#if 0
internal void print_line(line_info *l) {
  printf("index: %d\n", l->index);
  printf("count: %d\n", l->cells.count);
  printf("flags: %d\n", l->flags);
  printf(" --- chars:\n");
  for (u32 i = 0; i < l->cells.count; i++) {
    print_cell(l->cells.cells + i, "    ");
    printf(" ---\n");
  }
}

internal void print_all_lines(line_iterator *it) {
  b32 ok = 0;
  line_info info;
  b32 nok = next_line(it, &info);
  do {
    ok = nok;
    printf("=============\n");
    print_line(&info);
    nok = next_line(it, &info);
  } while(ok);
}
#endif

internal void basic_ops() {
  log_info("Testing basic ops");
  scrollback_buffer sbb;
  if (!allocate_scrollback_buffer(&sbb, 1024*16, 8192)) {
    log_error("Failed to allocate scrollback buffer");
    return;
  }
  character_cell entries[4] = {
    {.c = {.rune = 'a'}}, {.c = {.rune = 'b'}}, {.c = {.rune = 'c'}}, {.c = {.rune = 'd'}}
  };
  character_cell_buffer eb = {
    .count = 4,
    .cells = entries
  };
  line_iterator it;
  get_line_iterator(&sbb, &it);
  //  print_all_lines(&it);
  append_cells(&sbb, &eb, 0);
  assert(sbb.lines[0].start == 0 && sbb.lines[0].end == 3 && (sbb.lines[0].flags & EOL) == 0);
  get_line_iterator(&sbb, &it);
  line_info info;
  auto ok = next_line(&it, &info);
  assert(!ok);
  assert((info.flags & EOL) == 0);
  assert(info.cells.count == 4);
  append_cells(&sbb, &eb, 1);
  assert(sbb.lines[0].start == 0 && sbb.lines[0].end == 7 && (sbb.lines[0].flags & EOL));
  get_line_iterator(&sbb,&it);
  ok = next_line(&it, &info);
  assert(ok);
  assert(!(info.flags & EOL));
  assert(info.cells.count == 0);
  splice_into_line(&sbb, 0, 7, &eb);
  assert(sbb.lines[0].end == 10);
  append_cells(&sbb, &eb, 0);
  append_cells(&sbb, &eb, 0);
  assert(sbb.lines[0].end == 10);
  assert(sbb.lines[1].end == 18);
  append_cells(&sbb, NULL, 1);
  assert(sbb.lines[0].end == 10);
  assert(sbb.lines[1].end == 18);
  assert(sbb.lines[2].end == 19);
  splice_into_line(&sbb, 0, 7, &eb);

  assert(sbb.lines[0].end == 10);
  assert(sbb.lines[1].end == 18);
  assert(sbb.lines[2].end == 19);
  splice_into_line(&sbb, 0, 10, &eb);
  assert(sbb.lines[0].end == 13);
  assert(sbb.lines[1].end == 21);
  assert(sbb.lines[2].end == 22);
  assert(sbb.top_line == 0);
  assert(sbb.bottom_line == 2);
  get_line_iterator(&sbb,&it);
  //  print_all_lines(&it);
  delete_scrollback_buffer(&sbb);
}

internal void wrapping() {
  log_info("Testing wrapping");
  log_info("... packing input nicely to the limit");
  scrollback_buffer sbb;
  if (!allocate_scrollback_buffer(&sbb, 1024*16, 128)) {
    log_error("Failed to allocate scrollback buffer");
    return;
  }
  character_cell entries4[4] = {
    {.c = {.rune = 'a'}}, {.c = {.rune = 'b'}}, {.c = {.rune = 'c'}}, {.c = {.rune = 'd'}}
  };
  character_cell entries5[5] = {
    {.c = {.rune = 'A'}}, {.c = {.rune = 'B'}}, {.c = {.rune = 'C'}}, {.c = {.rune = 'D'}}, {.c = {.rune = 'E'}}
  };
  character_cell_buffer eb4 = {
    .count = 4,
    .cells = entries4
  };
  character_cell_buffer eb5 = {
    .count = 5,
    .cells = entries5
  };
  // Fill it up
  const u32 lc = sbb.line_count;
  for (u32 i = 0; i < lc; i++) {
    append_cells(&sbb, &eb4, 1);
  }
  line_iterator it;
  line_info info;
  get_line_iterator(&sbb, &it);
  b32 ok = 0;
  u32 lines_counted = 0;
  b32 nok = next_line(&it, &info);
  do {
    lines_counted++;
    ok = nok;
    nok = next_line(&it, &info);
  } while(ok);
  assert(lines_counted == lc);
  for (u32 i = 0; i < lc; i++) {
    auto r = memcmp(sbb.cells + sbb.lines[i].start, entries4, eb4.count*sizeof(character_cell));
    if (r) {
      log_error("Failed to match expected character cells: %d - %d", i, r);
      assert(!r);
    }
  }

  log_info("... packing with non-nice blobs");
  /* Since we are appending cells and indicating an EOL we wind up
     with one more line that is empty. */
  reset_scrollback_buffer(&sbb);
  lines_counted = 0;
  for (u32 i = 0; i < lc; i++) {
    append_cells(&sbb, &eb5, 1);
  }
  get_line_iterator(&sbb, &it);
  nok = next_line(&it, &info);
  do {
    lines_counted++;
    ok = nok;
    nok = next_line(&it, &info);
  } while(ok);
  u32 expected = sbb.cells_size/(sizeof(character_cell)*eb5.count) + 1;
  log_debug("Expected lines: %d", expected);
  log_debug("Actual lines counted: %d", lines_counted);
  assert(lines_counted == expected);
  assert(sbb.lines[sbb.bottom_line].count == 0);
  u32 idx = sbb.top_line;
  // We don't check the "bottom line" in this loop since we know it is empty
  for (u32 i = lines_counted - 1; i > 0; i--) {
    auto r = memcmp(sbb.cells + sbb.lines[idx].start, entries5, eb5.count*sizeof(character_cell));
    if (r) {
      log_error("Failed to match expected character cells: %d - %d", idx, r);
      assert(!r);
    }
    idx = (idx + 1) % sbb.line_count;
  }
  delete_scrollback_buffer(&sbb);
}

internal void over_filling() {
  log_info("Testing over filling");
  log_info("... one big chunk");
  scrollback_buffer sbb;
  if (!allocate_scrollback_buffer(&sbb, 1024*16, 128)) {
    log_error("Failed to allocate scrollback buffer");
    return;
  }
  const u32 overcount = sbb.cell_count+100;
  character_cell *entries = (character_cell *) calloc(overcount, sizeof(character_cell));
  for (u32 i = 0; i < overcount; i++) {
    entries[i].c.rune = 33 + (i % 60);
  }

  character_cell_buffer eb = {
    .count = overcount,
    .cells = entries
  };
  // Fill it up
  append_cells(&sbb, &eb, 0);

  assert(sbb.bottom_line == sbb.top_line);
  assert(sbb.lines[sbb.bottom_line].end == sbb.lines[sbb.bottom_line].start);
  assert(sbb.lines[sbb.bottom_line].count == sbb.wrap);
  assert(memcmp(sbb.cells + sbb.lines[sbb.bottom_line].start, entries + (overcount - sbb.wrap), sbb.wrap) == 0);
  line_iterator it;
  line_info info;
  get_line_iterator(&sbb, &it);
  b32 ok = 0;
  u32 lines_counted = 0;
  b32 nok = next_line(&it, &info);
  do {
    lines_counted++;
    ok = nok;
    nok = next_line(&it, &info);
  } while(ok);
  assert(lines_counted == 1);
  
  log_info("... lots of little chunks");  
  reset_scrollback_buffer(&sbb);
  u32 delta = overcount / 10;
  u32 last_place = delta * 10;
  eb.count = delta;
  for (u32 i = 0; i < 10; i++) {
    eb.cells = entries + (i * delta);
    append_cells(&sbb, &eb, 0);
  }
  assert(sbb.bottom_line == sbb.top_line);
  assert(sbb.lines[sbb.bottom_line].end == sbb.lines[sbb.bottom_line].start);
  assert(sbb.lines[sbb.bottom_line].count == sbb.wrap);
  assert(memcmp(sbb.cells + sbb.lines[sbb.bottom_line].start, entries + (last_place - sbb.wrap), sbb.wrap) == 0);
  get_line_iterator(&sbb, &it);
  lines_counted = 0;
  do {
    lines_counted++;
    ok = nok;
    nok = next_line(&it, &info);
  } while(ok);
  assert(lines_counted == 1);

  free(entries);
  delete_scrollback_buffer(&sbb);
}


internal void splice_testing() {
  log_info("Testing splice");
  scrollback_buffer sbb;
  if (!allocate_scrollback_buffer(&sbb, 1024*16, 128)) {
    log_error("Failed to allocate scrollback buffer");
    return;
  }
  character_cell entries4[4] = {
    {.c = {.rune = 'a'}}, {.c = {.rune = 'b'}}, {.c = {.rune = 'c'}}, {.c = {.rune = 'd'}}
  };
  character_cell entries5[5] = {
    {.c = {.rune = 'A'}}, {.c = {.rune = 'B'}}, {.c = {.rune = 'C'}}, {.c = {.rune = 'D'}}, {.c = {.rune = 'E'}}
  };
  character_cell_buffer eb4 = {
    .count = 4,
    .cells = entries4
  };
  character_cell_buffer eb5 = {
    .count = 5,
    .cells = entries5
  };
  const u32 overcount = sbb.wrap+100;
  character_cell *entries = (character_cell *) calloc(overcount, sizeof(character_cell));
  for (u32 i = 0; i < overcount; i++) {
    entries[i].c.rune = 33 + (i % 60);
  }
  character_cell_buffer eb = {
    .count = overcount,
    .cells = entries
  };

  character_cell_buffer shorter_eb = {
    .count = overcount - 110,
    .cells = entries + 110
  };

  // Splice into empty buffer
  splice_into_line(&sbb, 0, 10, &eb4);
  assert(sbb.lines[sbb.top_line].start == 0);
  assert(sbb.lines[sbb.top_line].end == 13);
  assert(sbb.lines[sbb.top_line].count == 14);
  assert(sbb.top_line == sbb.bottom_line);

  splice_into_line(&sbb, 0, 6, &eb5);
  assert(sbb.lines[sbb.top_line].start == 0);
  assert(sbb.lines[sbb.top_line].end == 13);
  assert(sbb.lines[sbb.top_line].count == 14);
  assert(sbb.top_line == sbb.bottom_line);
  
  splice_into_line(&sbb, 0, 6, &eb);
  assert(sbb.lines[sbb.top_line].start == sbb.lines[sbb.top_line].end);
  assert(sbb.lines[sbb.top_line].count == sbb.wrap);
  assert(sbb.top_line == sbb.bottom_line);
  assert(available_lines(&sbb) == 1);

  splice_into_line(&sbb, 0, 6, &shorter_eb);
  assert(sbb.lines[sbb.top_line].start == sbb.lines[sbb.top_line].end);
  assert(sbb.lines[sbb.top_line].count == sbb.wrap);
  assert(sbb.top_line == sbb.bottom_line);
  assert(available_lines(&sbb) == 1);

  splice_into_line(&sbb, 0, 115, &shorter_eb);
  assert(sbb.lines[sbb.top_line].start == sbb.lines[sbb.top_line].end);
  assert(sbb.lines[sbb.top_line].count == sbb.wrap);
  assert(sbb.top_line == sbb.bottom_line);
  assert(available_lines(&sbb) == 1);

  append_cells(&sbb, &eb4, 1);
  assert(sbb.top_line + 1 == sbb.bottom_line);
  assert(available_lines(&sbb) == 2);
  append_cells(&sbb, &eb, 0);
  assert(sbb.top_line == sbb.bottom_line);
  assert(available_lines(&sbb) == 1);

  append_cells(&sbb, NULL, 1);
  assert(sbb.top_line + 1 == sbb.bottom_line);
  assert(available_lines(&sbb) == 2);
  append_cells(&sbb, &eb4, 0);
  assert(sbb.top_line == sbb.bottom_line);
  assert(available_lines(&sbb) == 1);

  reset_scrollback_buffer(&sbb);
  append_cells(&sbb, &shorter_eb, 0);
  assert(available_lines(&sbb) == 1);
  splice_into_line(&sbb, 0, 110, &shorter_eb);
  assert(available_lines(&sbb) == 1);
  assert(sbb.lines[sbb.top_line].start == sbb.lines[sbb.top_line].end);
  assert(sbb.lines[sbb.top_line].count == sbb.wrap);
  assert(sbb.top_line == sbb.bottom_line);
  auto wrapped_amount = (shorter_eb.count+110)-sbb.wrap;
  auto remaining_amount = 110 - wrapped_amount;
  assert(!memcmp(shorter_eb.cells + wrapped_amount, sbb.cells + sbb.lines[sbb.top_line].start, remaining_amount * sizeof(character_cell)));
  assert(!memcmp(shorter_eb.cells, sbb.cells + sbb.lines[sbb.top_line].start + remaining_amount, shorter_eb.count * sizeof(character_cell)));
  
  free(entries);
  delete_scrollback_buffer(&sbb);
}


int main(int argc, char **argv) {
  basic_ops();
  wrapping();
  over_filling();
  splice_testing();
}
