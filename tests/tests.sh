#!/usr/bin/env bash

font_flags="-lfreetype -I /usr/include/freetype2"
warning_flags="-Wall -Wextra -Wno-unused-parameter -Wno-unused-result -Wno-missing-field-initializers"

for x in *_test.cpp; do
    echo "---- Build - $x:"
    clang "$x" -o ${x%.cpp} -g -O0 -lm -lX11 -pthread $warning_flags $font_flags $extra_flags
    echo "      -------- Test - $x:"
    ./${x%.cpp}
    rm -f ${x%.cpp}
    echo "      --------     "
done
